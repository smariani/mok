/**
 * Created by Stefano Mariani on 31/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class ConcentrationTest {

    private Concentration concentration1;
    private Concentration concentration2;
    private Concentration concentration3;

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @Before
    public final void setUp() throws Exception {
        this.concentration1 = new Concentration();
        this.concentration2 = new Concentration(2);
        this.concentration3 = new Concentration(3);
    }

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @After
    public final void tearDown() throws Exception {
        this.concentration1 = null;
        this.concentration2 = null;
        this.concentration3 = null;
    }

    /**
     * Test method for
     * {@link mok.model.Concentration#compareTo(mok.model.IConcentration)}.
     */
    @Test
    public final void testCompareTo() {
        final List<Concentration> cs1 = new LinkedList<>();
        cs1.add(this.concentration3);
        cs1.add(this.concentration2);
        cs1.add(this.concentration1);
        final List<Concentration> cs2 = new LinkedList<>();
        for (final Concentration c : cs1) {
            cs2.add(new Concentration(c.toLong()));
        }
        Collections.sort(cs2);
        Assert.assertNotEquals(cs1, cs2);
        final List<Concentration> cs3 = new LinkedList<>();
        cs3.add(this.concentration1);
        cs3.add(this.concentration2);
        cs3.add(this.concentration3);
        Assert.assertEquals(cs2, cs3);
        Assert.assertTrue(
                this.concentration1.compareTo(new Concentration()) == 0);
        Assert.assertTrue(
                this.concentration2.compareTo(this.concentration1) > 0);
        Assert.assertTrue(
                this.concentration2.compareTo(this.concentration3) < 0);
    }

    /**
     * Test method for {@link mok.model.Concentration#decConcentration(long)}.
     */
    @Test
    public final void testDecConcentration() {
        this.concentration1.decConcentration(1);
        Assert.assertTrue(this.concentration1.toLong() == 0);
        this.concentration1.decConcentration(10);
        Assert.assertTrue(this.concentration1.toLong() == 0);
        this.concentration2.decConcentration(1);
        Assert.assertTrue(this.concentration2.toLong() == 1);
        this.concentration2.decConcentration(2);
        Assert.assertTrue(this.concentration2.toLong() == 0);
        this.concentration2.decConcentration(10);
        Assert.assertTrue(this.concentration2.toLong() == 0);
        this.concentration3.decConcentration(0);
        Assert.assertTrue(this.concentration3.toLong() == 3);
        this.concentration3.decConcentration(1);
        Assert.assertTrue(this.concentration3.toLong() == 2);
        this.concentration3.decConcentration(2);
        Assert.assertTrue(this.concentration3.toLong() == 0);
        this.concentration3.decConcentration(3);
        Assert.assertTrue(this.concentration3.toLong() == 0);
    }

}
