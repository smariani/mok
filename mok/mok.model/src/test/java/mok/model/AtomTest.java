/**
 * Created by Stefano Mariani on 27/mag/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.net.URI;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class AtomTest {

    private IAtom atom1;
    private IAtom atom2;
    private IAtom atom3;
    private IAtom atom4;

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @Before
    public final void setUp() throws Exception {
        this.atom1 = new Atom(
                new Seed(new Source(URI.create("it.unibo.fake/uri"))),
                new Content("content"));
        this.atom2 = new Atom(
                new Seed(new Source(URI.create("it.unibo.fake/uri"))),
                new Content("content"), new Meta());
        this.atom3 = new Atom(
                new Seed(new Source(URI.create("it.unibo.fake/uri"))),
                new Content("content"), new Meta(), new Concentration());
        this.atom4 = new Atom(
                new Seed(new Source(URI.create("it.unibo.fake/uri"),
                        "fake uri")),
                new Content("content"), new Meta(), new Concentration());
    }

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @After
    public final void tearDown() throws Exception {
        this.atom1 = null;
        this.atom2 = null;
        this.atom3 = null;
        this.atom4 = null;
    }

    /**
     * Test method for {@link mok.model.Atom#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        Assert.assertEquals(this.atom1, this.atom1);
        Assert.assertEquals(this.atom1, this.atom2);
    }

    /**
     * Test method for {@link mok.model.Atom#getConcentration()}.
     */
    @Test
    public final void testGetConcentration() {
        Assert.assertEquals(this.atom1.getConcentration(), new Concentration());
        Assert.assertEquals(this.atom2.getConcentration(), new Concentration());
        Assert.assertEquals(this.atom3.getConcentration(), new Concentration());
        Assert.assertEquals(this.atom1.getConcentration(),
                this.atom2.getConcentration());
        Assert.assertEquals(this.atom1.getConcentration(),
                this.atom3.getConcentration());
        Assert.assertEquals(this.atom2.getConcentration(),
                this.atom3.getConcentration());
    }

    /**
     * Test method for {@link mok.model.Atom#getContent()}.
     */
    @Test
    public final void testGetContent() {
        Assert.assertEquals(this.atom1.getContent(), new Content("content"));
        Assert.assertEquals(this.atom2.getContent(), new Content("content"));
        Assert.assertEquals(this.atom3.getContent(), new Content("content"));
        Assert.assertEquals(this.atom1.getContent(), this.atom2.getContent());
        Assert.assertEquals(this.atom1.getContent(), this.atom3.getContent());
        Assert.assertEquals(this.atom2.getContent(), this.atom3.getContent());
    }

    /**
     * Test method for {@link mok.model.Atom#getMeta()}.
     */
    @Test
    public final void testGetMeta() {
        Assert.assertEquals(this.atom1.getMeta(), new Meta());
        Assert.assertEquals(this.atom2.getMeta(), new Meta());
        Assert.assertEquals(this.atom3.getMeta(), new Meta());
        Assert.assertEquals(this.atom1.getMeta(), this.atom2.getMeta());
        Assert.assertEquals(this.atom1.getMeta(), this.atom3.getMeta());
        Assert.assertEquals(this.atom2.getMeta(), this.atom3.getMeta());
    }

    /**
     * Test method for {@link mok.model.Atom#getSeed()}.
     */
    @Test
    public final void testGetSeed() {
        Assert.assertEquals(this.atom1.getSeed(),
                new Seed(new Source(URI.create("it.unibo.fake/uri"))));
        Assert.assertEquals(this.atom2.getSeed(),
                new Seed(new Source(URI.create("it.unibo.fake/uri"))));
        Assert.assertEquals(this.atom3.getSeed(),
                new Seed(new Source(URI.create("it.unibo.fake/uri"))));
        Assert.assertEquals(this.atom4.getSeed(), new Seed(
                new Source(URI.create("it.unibo.fake/uri"), "fake URI")));
        Assert.assertEquals(this.atom1.getSeed(), this.atom2.getSeed());
        Assert.assertEquals(this.atom1.getSeed(), this.atom3.getSeed());
        Assert.assertEquals(this.atom2.getSeed(), this.atom3.getSeed());
        Assert.assertNotEquals(this.atom3.getSeed(), this.atom4.getSeed());
    }

    /**
     * Test method for {@link java.lang.Object#toString()}.
     */
    @Test
    public final void testToString() {
        Assert.assertEquals(this.atom1.toString(),
                "atom: [ seed: source: it.unibo.fake/uri, content: content, meta:  ](1)");
        Assert.assertEquals(this.atom2.toString(),
                "atom: [ seed: source: it.unibo.fake/uri, content: content, meta:  ](1)");
        Assert.assertEquals(this.atom3.toString(),
                "atom: [ seed: source: it.unibo.fake/uri, content: content, meta:  ](1)");
        Assert.assertEquals(this.atom4.toString(),
                "atom: [ seed: source: it.unibo.fake/uri, fake uri, content: content, meta:  ](1)");
        Assert.assertEquals(this.atom1.toString(), this.atom2.toString());
        Assert.assertEquals(this.atom1.toString(), this.atom3.toString());
        Assert.assertEquals(this.atom2.toString(), this.atom3.toString());
    }
}
