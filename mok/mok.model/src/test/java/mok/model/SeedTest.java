/**
 * Created by Stefano Mariani on 29/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashSet;
import java.util.Set;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class SeedTest {

    private IAtom atom1;
    private IAtom atom2;
    private IOffspring injection1;
    private IOffspring injection2;
    private Set<IOffspring> injections;
    private ISeed seed1;
    private ISeed seed2;

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @Before
    public final void setUp() throws Exception {
        this.seed1 = new Seed(new Source(new URI("it.unibo.fake/uri")));
        this.injections = new LinkedHashSet<>();
        this.atom1 = new Atom(this.seed1, new Content("content1"));
        this.injection1 = new Offspring(this.atom1, new Rate(0.5),
                new Concentration(10));
        this.injections.add(this.injection1);
        this.atom2 = new Atom(this.seed1, new Content("content2"));
        this.injection2 = new Offspring(this.atom2, new Rate(0.05),
                new Concentration(20));
        this.injections.add(this.injection2);
        this.seed2 = new Seed(new Source(new URI("it.unibo.fake/uri2")),
                this.injections);
    }

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @After
    public final void tearDown() throws Exception {
        this.seed1 = null;
        this.seed2 = null;
        this.atom1 = null;
        this.atom2 = null;
        this.injection1 = null;
        this.injection2 = null;
        this.injections = null;
    }

    /**
     * Test method for {@link mok.model.Seed#addOffspring(mok.model.IOffspring)} .
     */
    @Test
    public final void testAddInjection() {
        this.seed1.addOffspring(this.injection1);
        Assert.assertTrue(this.seed1.getOffsprings().size() == 1);
        for (final IOffspring inj : this.seed1.getOffsprings()) {
            Assert.assertEquals(inj, this.injection1);
        }
        final IOffspring injection3 = new Offspring(
                new Atom(this.seed1, new Content("content3")), new Rate(0.005),
                new Concentration(30));
        this.seed2.addOffspring(injection3);
        this.seed2.addOffspring(this.injection1);
        Assert.assertTrue(this.seed2.getOffsprings().size() == 3);
        final IOffspring[] injs = this.seed2.getOffsprings()
                .toArray(new IOffspring[] {});
        Assert.assertEquals(injs[0], this.injection1);
        Assert.assertEquals(injs[1], this.injection2);
        Assert.assertEquals(injs[2], injection3);
    }

    /**
     * Test method for {@link mok.model.Seed#getAtoms()}.
     */
    @Test
    public final void testGetAtoms() {
        for (final IAtom a : this.seed2.getAtoms()) {
            Assert.assertTrue(this.seed2.getAtoms().contains(a));
        }
        final IAtom[] as = this.seed2.getAtoms().toArray(new IAtom[] {});
        Assert.assertEquals(as[0], this.atom1);
        Assert.assertEquals(as[1], this.atom2);
    }

    /**
     * Test method for {@link mok.model.Seed#getOffsprings()}.
     */
    @Test
    public final void testGetInjections() {
        for (final IOffspring i : this.seed2.getOffsprings()) {
            Assert.assertTrue(this.seed2.getOffsprings().contains(i));
        }
        final IOffspring[] injs = this.seed2.getOffsprings()
                .toArray(new IOffspring[] {});
        Assert.assertEquals(injs[0], this.injection1);
        Assert.assertEquals(injs[1], this.injection2);
    }

    /**
     * Test method for {@link mok.model.Seed#getSource()}.
     */
    @Test
    public final void testGetSource() {
        try {
            Assert.assertEquals(this.seed1.getSource(),
                    new Source(new URI("it.unibo.fake/uri")));
            Assert.assertEquals(this.seed2.getSource(),
                    new Source(new URI("it.unibo.fake/uri2")));
        } catch (final URISyntaxException e) {
            /**
             * Cannot really happen
             */
        }
        Assert.assertNotEquals(this.seed1.getSource(), this.seed2.getSource());
    }

    /**
     * Test method for {@link mok.model.Seed#removeOffspring(mok.model.IOffspring)}
     * .
     */
    @Test
    public final void testRemoveInjection() {
        this.seed1.removeOffspring(this.injection1);
        Assert.assertTrue(this.seed1.getOffsprings().size() == 0);
        this.seed2.removeOffspring(this.injection1);
        Assert.assertTrue(this.seed2.getOffsprings().size() == 1);
        Assert.assertFalse(
                this.seed2.getOffsprings().contains(this.injection1));
        Assert.assertTrue(this.seed2.getOffsprings().contains(this.injection2));
    }

    /**
     * Test method for {@link mok.model.Seed#setInjections(java.util.Set)}.
     */
    @Test
    public final void testSetInjections() {
        this.seed1.setInjections(this.injections);
        Assert.assertTrue(this.seed2.getOffsprings().size() == 2);
        for (final IOffspring i : this.injections) {
            Assert.assertTrue(this.seed1.getOffsprings().contains(i));
        }
        final IOffspring[] injs = this.seed1.getOffsprings()
                .toArray(new IOffspring[] {});
        Assert.assertEquals(injs[0], this.injection1);
        Assert.assertEquals(injs[1], this.injection2);
    }

    /**
     * Test method for {@link mok.model.Seed#toString()}.
     */
    @Test
    public final void testToString() {
        Assert.assertEquals(this.seed2.toString(),
                "seed: [ " + this.seed2.getSource() + ", injections: { "
                        + this.injection1 + " | " + this.injection2 + "  } ]");
    }

}
