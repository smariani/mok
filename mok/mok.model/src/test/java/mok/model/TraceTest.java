/**
 * Created by Stefano Mariani on 29/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.net.URI;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class TraceTest {

    private IEnzyme enzyme1;
    private IEnzyme enzyme2;
    private IPerturbationAction perturbation1;
    private IPerturbationAction perturbation2;
    private ITrace trace1;
    private ITrace trace2;
    private ITrace trace3;

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @Before
    public final void setUp() throws Exception {
        final IAtom atom = new Atom(
                new Seed(new Source(new URI("it.unibo.fake/uri"))),
                new Content("content"));
        this.perturbation1 = new AbstractPerturbationAction(atom, "foo") {

            @Override
            public boolean apply() {
                this.getReactantTemplate().getConcentration()
                        .incConcentration(Concentration.MIN_CONC);
                return false;
            }
        };
        this.perturbation2 = new AbstractPerturbationAction(atom, "fuu") {

            @Override
            public boolean apply() {
                this.getReactantTemplate().getConcentration()
                        .decConcentration(Concentration.MIN_CONC);
                return false;
            }
        };
        this.enzyme1 = new Enzyme(atom, ESpecies.FAVOURITE);
        this.trace1 = new Trace(this.enzyme1, this.perturbation1);
        this.enzyme2 = new Enzyme(atom, ESpecies.FOLLOW);
        this.trace2 = new Trace(this.enzyme2, this.perturbation2,
                new Concentration(2));
        this.trace3 = new Trace(this.enzyme2,
                new AbstractPerturbationAction(atom, "FUU") {

                    @Override
                    public boolean apply() {
                        this.getReactantTemplate().getConcentration()
                                .decConcentration(Concentration.MIN_CONC);
                        return false;
                    }
                });
    }

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @After
    public final void tearDown() throws Exception {
        this.trace1 = null;
        this.trace2 = null;
        this.trace3 = null;
        this.enzyme1 = null;
        this.enzyme2 = null;
        this.perturbation1 = null;
        this.perturbation2 = null;
    }

    /**
     * Test method for {@link mok.model.Trace#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        Assert.assertNotEquals(this.trace1, this.trace2);
        Assert.assertEquals(this.trace3, this.trace2);
    }

    /**
     * Test method for {@link mok.model.Trace#getConcentration()}.
     */
    @Test
    public final void testGetConcentration() {
        Assert.assertEquals(this.trace1.getConcentration(),
                new Concentration());
        Assert.assertEquals(this.trace2.getConcentration(),
                new Concentration(2));
        Assert.assertTrue(this.trace1.getConcentration().toLong() == 1);
        Assert.assertTrue(this.trace2.getConcentration().toLong() == 2);
    }

    /**
     * Test method for {@link mok.model.Trace#getEnzymeTemplate()}.
     */
    @Test
    public final void testGetEnzyme() {
        Assert.assertEquals(this.trace1.getEnzymeTemplate(), this.enzyme1);
        Assert.assertEquals(this.trace2.getEnzymeTemplate(), this.enzyme2);
        Assert.assertNotEquals(this.trace1.getEnzymeTemplate(), this.enzyme2);
        Assert.assertNotEquals(this.trace2.getEnzymeTemplate(), this.enzyme1);
    }

    /**
     * Test method for {@link mok.model.Trace#getPerturbationAction()}.
     */
    @Test
    public final void testGetPerturbation() {
        Assert.assertNotEquals(this.trace1.getPerturbationAction(),
                this.trace2.getPerturbationAction());
        Assert.assertEquals(this.trace1.getPerturbationAction(), this.perturbation1);
        Assert.assertEquals(this.trace2.getPerturbationAction(), this.perturbation2);
    }

    /**
     * Test method for {@link mok.model.Trace#toString()}.
     */
    @Test
    public final void testToString() {
        Assert.assertEquals(this.trace1.toString(),
                "trace: [ enzyme: " + this.trace1.getEnzymeTemplate()
                        + ", perturbation: "
                        + this.trace1.getPerturbationAction().getDescription() + " ]("
                        + this.trace1.getConcentration() + ")");
        Assert.assertEquals(this.trace2.toString(),
                "trace: [ enzyme: " + this.trace2.getEnzymeTemplate()
                        + ", perturbation: "
                        + this.trace2.getPerturbationAction().getDescription() + " ]("
                        + this.trace2.getConcentration() + ")");
        Assert.assertNotEquals(this.trace1.toString(), this.trace2.toString());
    }

}
