/**
 * Created by Stefano Mariani on 30/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.net.URI;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class InjectionTest {

    private IAtom atom1;
    private IAtom atom2;
    private IOffspring injection1;
    private IOffspring injection2;
    private IOffspring injection3;

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @Before
    public final void setUp() throws Exception {
        this.atom1 = new Atom(
                new Seed(new Source(new URI("it.unibo.fake/uri"))),
                new Content("content"));
        this.injection1 = new Offspring(this.atom1, new Rate(0.5),
                new Concentration());
        this.atom2 = new Atom(
                new Seed(new Source(new URI("it.unibo.fake/uri2"))),
                new Content("content2"));
        this.injection2 = new Offspring(this.atom2, new Rate(0.05),
                new Concentration());
        this.injection3 = new Offspring(this.atom1, new Rate(0.05),
                new Concentration(2));
    }

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @After
    public final void tearDown() throws Exception {
        this.injection1 = null;
        this.injection2 = null;
        this.injection3 = null;
        this.atom1 = null;
        this.atom2 = null;
    }

    /**
     * Test method for {@link mok.model.Offspring#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        Assert.assertNotEquals(this.injection1, this.injection2);
        Assert.assertEquals(this.injection1, this.injection3);
    }

    /**
     * Test method for {@link mok.model.Offspring#getAtom()}.
     */
    @Test
    public final void testGetAtom() {
        Assert.assertEquals(this.injection1.getAtom(), this.atom1);
        Assert.assertEquals(this.injection2.getAtom(), this.atom2);
        Assert.assertEquals(this.injection3.getAtom(), this.atom1);
        Assert.assertNotEquals(this.injection1.getAtom(), this.atom2);
        Assert.assertNotEquals(this.injection3.getAtom(), this.atom2);
    }

    /**
     * Test method for {@link mok.model.Offspring#getConcentration()}.
     */
    @Test
    public final void testGetConcentration() {
        Assert.assertEquals(this.injection1.getConcentration(),
                new Concentration());
        Assert.assertEquals(this.injection2.getConcentration(),
                new Concentration());
        Assert.assertEquals(this.injection3.getConcentration(),
                new Concentration(2));
        Assert.assertNotEquals(this.injection1.getConcentration(),
                this.injection3.getConcentration());
        Assert.assertNotEquals(this.injection2.getConcentration(),
                this.injection3.getConcentration());
    }

    /**
     * Test method for {@link mok.model.Offspring#getRate()}.
     */
    @Test
    public final void testGetRate() {
        Assert.assertEquals(this.injection1.getRate(), new Rate(0.5));
        Assert.assertEquals(this.injection2.getRate(), new Rate(0.05));
        Assert.assertEquals(this.injection3.getRate(), new Rate(0.05));
        Assert.assertNotEquals(this.injection1.getRate(),
                this.injection3.getRate());
        Assert.assertNotEquals(this.injection1.getRate(),
                this.injection2.getRate());
        Assert.assertEquals(this.injection2.getRate(),
                this.injection3.getRate());
        Assert.assertTrue(this.injection1.getRate().toDouble() == 0.5);
        Assert.assertTrue(this.injection2.getRate().toDouble() == 0.05);
        Assert.assertTrue(this.injection3.getRate().toDouble() == 0.05);
        Assert.assertFalse(this.injection1.getRate()
                .toDouble() == this.injection3.getRate().toDouble());
        Assert.assertFalse(this.injection1.getRate()
                .toDouble() == this.injection2.getRate().toDouble());
        Assert.assertTrue(this.injection2.getRate()
                .toDouble() == this.injection3.getRate().toDouble());
    }

    /**
     * Test method for {@link mok.model.Offspring#toString()}.
     */
    @Test
    public final void testToString() {
        Assert.assertEquals(this.injection1.toString(),
                this.injection1.getConcentration() + " * atom: [ content: "
                        + this.atom1.getContent() + ", meta: "
                        + this.atom1.getMeta() + " ] @ "
                        + this.injection1.getRate());
        Assert.assertNotEquals(this.injection2.toString(),
                this.injection3.toString());
    }
}
