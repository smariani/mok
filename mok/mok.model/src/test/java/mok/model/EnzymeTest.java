/**
 * Created by Stefano Mariani on 29/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.net.URI;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class EnzymeTest {

    private IAtom atom1;
    private IEnzyme enzyme1;
    private IEnzyme enzyme2;
    private IEnzyme enzyme3;
    private IEnzyme enzyme4;
    private IMolecule molecule1;

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @Before
    public final void setUp() throws Exception {
        this.atom1 = new Atom(
                new Seed(new Source(new URI("it.unibo.fake/uri"))),
                new Content("content"));
        this.enzyme1 = new Enzyme(this.atom1, ESpecies.FAVOURITE);
        this.molecule1 = new Molecule(this.atom1,
                new Atom(new Seed(new Source(new URI("it.unibo.fake/uri2"))),
                        new Content("content2")));
        this.enzyme2 = new Enzyme(this.molecule1, ESpecies.FOLLOW,
                new Concentration(2));
        this.enzyme3 = new Enzyme(this.atom1, ESpecies.LIKE,
                new Concentration(), new Concentration(2));
        this.enzyme4 = new Enzyme(this.atom1, ESpecies.FAVOURITE,
                new Concentration(2), new Concentration(3));
    }

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @After
    public final void tearDown() throws Exception {
        this.enzyme1 = null;
        this.enzyme2 = null;
        this.enzyme3 = null;
        this.enzyme4 = null;
        this.atom1 = null;
        this.molecule1 = null;
    }

    /**
     * Test method for {@link mok.model.Enzyme#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        Assert.assertNotEquals(this.enzyme1, this.enzyme2);
        Assert.assertNotEquals(this.enzyme2, this.enzyme3);
        Assert.assertNotEquals(this.enzyme1, this.enzyme3);
        Assert.assertEquals(this.enzyme1, this.enzyme4);
    }

    /**
     * Test method for {@link mok.model.Enzyme#getConcentration()}.
     */
    @Test
    public final void testGetConcentration() {
        Assert.assertEquals(this.enzyme1.getConcentration(),
                new Concentration());
        Assert.assertEquals(this.enzyme2.getConcentration(),
                new Concentration());
        Assert.assertEquals(this.enzyme3.getConcentration(),
                new Concentration(2));
    }

    /**
     * Test method for {@link mok.model.Enzyme#getReactantTemplate()}.
     */
    @Test
    public final void testGetReactant() {
        Assert.assertEquals(this.enzyme1.getReactantTemplate(), this.atom1);
        Assert.assertEquals(this.enzyme2.getReactantTemplate(), this.molecule1);
    }

    /**
     * Test method for {@link mok.model.Enzyme#getSpecies()}.
     */
    @Test
    public final void testGetSpecies() {
        Assert.assertEquals(this.enzyme1.getSpecies(), ESpecies.FAVOURITE);
        Assert.assertEquals(this.enzyme2.getSpecies(), ESpecies.FOLLOW);
        Assert.assertEquals(this.enzyme3.getSpecies(), ESpecies.LIKE);
    }

    /**
     * Test method for {@link mok.model.Enzyme#getStrength()}.
     */
    @Test
    public final void testGetStrength() {
        Assert.assertEquals(this.enzyme1.getStrength(), new Concentration());
        Assert.assertEquals(this.enzyme2.getStrength(), new Concentration(2));
        Assert.assertEquals(this.enzyme3.getStrength(), new Concentration(1));
    }

    /**
     * Test method for {@link mok.model.Enzyme#toString()}.
     */
    @Test
    public final void testToString() {
        Assert.assertEquals(this.enzyme1.toString(),
                "enzyme: [ reactant: " + this.enzyme1.getReactantTemplate()
                        + ", species: " + this.enzyme1.getSpecies()
                        + ", strength: " + this.enzyme1.getStrength()
                        + " ](1)");
    }
}
