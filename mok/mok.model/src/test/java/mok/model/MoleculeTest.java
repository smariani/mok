/**
 * Created by Stefano Mariani on 28/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.net.URI;
import java.util.LinkedHashSet;
import java.util.Set;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class MoleculeTest {

    private IAtom atom2;
    private IAtom atom3;
    private IAtom atom4;
    private Set<IAtom> atoms;
    private IMolecule molecule2;
    private IMolecule molecule3;
    private IMolecule molecule4;

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @SuppressWarnings("unused")
    @Before
    public final void setUp() throws Exception {
        final IAtom atom1 = new Atom(
                new Seed(new Source(URI.create("it.unibo.fake/uri"))),
                new Content("content"));
        this.atom2 = new Atom(
                new Seed(new Source(URI.create("it.unibo.fake/uri"))),
                new Content("content"), new Meta());
        this.atom3 = new Atom(
                new Seed(new Source(URI.create("it.unibo.fake/uri3"))),
                new Content("content3"), new Meta("meta3"),
                new Concentration(3000));
        try {
            new Molecule(atom1, this.atom2);
            Assert.fail("Expected IllegalArgumentException was not thrown :/");
        } catch (final IllegalArgumentException e) {
            /**
             * Expected exception succesfully thrown :)
             */
        }
        this.molecule2 = new Molecule(atom1, this.atom3);
        this.atom4 = new Atom(
                new Seed(new Source(URI.create("it.unibo.fake/uri4"))),
                new Content("content4"), new Meta("meta4"),
                new Concentration(4000));
        this.atoms = new LinkedHashSet<>();
        this.atoms.add(this.atom2);
        this.atoms.add(this.atom3);
        this.atoms.add(this.atom4);
        this.molecule3 = new Molecule(this.atoms);
        this.molecule4 = new Molecule(this.atoms);
    }

    /**
     * @throws java.lang.Exception
     *             (required by JUnit)
     */
    @After
    public final void tearDown() throws Exception {
        this.molecule2 = null;
        this.molecule3 = null;
        this.molecule4 = null;
        this.atom2 = null;
        this.atom3 = null;
        this.atom4 = null;
        this.atoms = null;
    }

    /**
     * Test method for {@link mok.model.Molecule#addAtom(mok.model.IAtom)}.
     */
    @Test
    public final void testAddAtom() {
        this.molecule2.addAtom(this.atom4);
        Assert.assertEquals(this.molecule2.getAtoms(), this.atoms);
        Assert.assertEquals(this.molecule2.getConcentration(),
                new Concentration());
        this.molecule3.addAtom(this.atom4);
        final Set<IAtom> as = new LinkedHashSet<>();
        as.add(this.atom2);
        as.add(this.atom3);
        as.add(new Atom(new Seed(new Source(URI.create("it.unibo.fake/uri4"))),
                new Content("content4"), new Meta("meta4"),
                new Concentration(2)));
        Assert.assertEquals(this.molecule3.getAtoms(), as);
        Assert.assertEquals(this.molecule3.getConcentration(),
                new Concentration(2));
    }

    /**
     * Test method for {@link mok.model.Molecule#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        Assert.assertNotEquals(this.molecule2, this.molecule3);
        Assert.assertEquals(this.molecule4, this.molecule3);
        this.molecule4.addAtoms(this.atoms);
        Assert.assertEquals(this.molecule4, this.molecule3);
    }

    /**
     * Test method for {@link mok.model.Molecule#getAtoms()}.
     */
    @Test
    public final void testGetAtoms() {
        Assert.assertEquals(this.molecule3.getAtoms(), this.atoms);
    }

    /**
     * Test method for {@link mok.model.Molecule#getConcentration()}.
     */
    @Test
    public final void testGetConcentration() {
        Assert.assertEquals(this.molecule3.getConcentration(),
                new Concentration(1));
        Assert.assertEquals(this.molecule3.getConcentration().toLong(),
                new Concentration(1).toLong());
        Assert.assertTrue(this.molecule3.getConcentration().toLong() == 1);
        this.molecule3.addAtom(this.atom4);
        Assert.assertEquals(this.molecule3.getConcentration(),
                new Concentration(2));
        Assert.assertEquals(this.molecule3.getConcentration().toLong(),
                new Concentration(2).toLong());
        Assert.assertTrue(this.molecule3.getConcentration().toLong() == 2);
    }

    /**
     * Test method for {@link mok.model.Molecule#toString()}.
     */
    @Test
    public final void testToString() {
        this.atom3.getConcentration().setConcentration(1);
        Assert.assertEquals(this.molecule2.toString(),
                "molecule: { " + this.atom2 + " | " + this.atom3 + "  }(1)");
    }

}
