/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class TraceTemplate implements ITraceTemplate {

    private final IConcentration concentration;
    private final IEnzymeTemplate enzymeTemplate;
    private final IPerturbationActionTemplate perturbationTemplate;

    /**
     * Minimal constructor, builds {@code this} with the given
     * {@link IEnzymeTemplate} and the given {@link IPerturbationActionTemplate}
     * .
     *
     * @param et
     *            the enzyme template {@code this} considers.
     * @param pt
     *            the perturbation action template {@code this} considers.
     */
    public TraceTemplate(final IEnzymeTemplate et,
            final IPerturbationActionTemplate pt) {
        if ((et == null) || (pt == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.enzymeTemplate = et;
        this.perturbationTemplate = pt;
        this.concentration = new Concentration();
    }

    /**
     * Complete constructor, builds {@code this} also with the given minimal
     * required {@link IConcentration}.
     *
     * @param et
     *            the {@link IEnzymeTemplate} {@code this} considers.
     * @param pt
     *            the {@link IPerturbationActionTemplate} {@code this}
     *            considers.
     * @param c
     *            the minimal required concentration of {@code this}.
     */
    public TraceTemplate(final IEnzymeTemplate et,
            final IPerturbationActionTemplate pt, final IConcentration c) {
        if ((et == null) || (pt == null) || (c == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.enzymeTemplate = et;
        this.perturbationTemplate = pt;
        this.concentration = c;
    }

    /* (non-Javadoc)
     * @see mok.model.IReactantTemplate#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IReactantTemplate decConcentration(final IConcentration decrement) {
        // TODO Null check
        this.concentration.decConcentration(decrement.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentrationTemplate#checkMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatchingDegree checkMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentrationTemplate#doMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatch doMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IProductTemplate#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IProductTemplate incConcentration(final IConcentration increment) {
        // TODO Null check
        this.concentration.incConcentration(increment.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.ITraceTemplate#getEnzymeTemplate()
     */
    @Override
    public IEnzymeTemplate getEnzymeTemplate() {
        return this.enzymeTemplate;
    }

    /* (non-Javadoc)
     * @see mok.model.ITraceTemplate#getPerturbationTemplate()
     */
    @Override
    public IPerturbationActionTemplate getPerturbationTemplate() {
        return this.perturbationTemplate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ITraceTemplate) {
            final ITraceTemplate o = (ITraceTemplate) obj;
            if (o.getEnzymeTemplate().equals(this.enzymeTemplate)
                    && o.getPerturbationTemplate()
                            .equals(this.perturbationTemplate)) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.enzymeTemplate.hashCode();
        res = (31 * res) + this.perturbationTemplate.hashCode();
        return res;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format(
                "trace template: [ enzyme: %s, perturbation: %s ](%s)",
                this.enzymeTemplate,
                this.getPerturbationTemplate().getDescriptionTemplate(),
                this.concentration);
    }

}
