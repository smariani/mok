/**
 * Created by Stefano Mariani on 19/giu/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.LinkedHashSet;
import java.util.Set;

/*
 * TODO Implement custom serialized form (as part of Atom), how to modify
 * offsprings' parameters given atom? implement clone() or factory constructor
 */
/**
 * <p>
 * Implementation class for {@link ISeed}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Seed implements ISeed {

    private IConcentration concentration;
    private final Set<IOffspring> injections;
    private final ISource source;

    /**
     * Minimal constructor, builds {@code this} with no {@link IOffspring}s and
     * minimal initial {@link IConcentration}.
     *
     * @param src
     *            the {@link ISource} {@code this} represents.
     */
    public Seed(final ISource src) {
        if (src == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.source = src;
        this.injections = new LinkedHashSet<>();
        this.concentration = new Concentration();
    }

    /**
     * Minimal-concentration constructor, builds {@code this} with the given
     * {@link IOffspring} and minimal {@link IConcentration}.
     *
     * @param src
     *            the {@link ISource} {@code this} represents.
     * @param injs
     *            the set of offsprings {@code this} generates.
     */
    public Seed(final ISource src, final Set<IOffspring> injs) {
        this(src);
        if (injs == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.injections.addAll(injs);
    }

    /**
     * Complete constructor, builds {@code this} representing the given
     * {@link ISource}, injecting the given {@link IOffspring} and having the
     * given initial {@link IConcentration}.
     *
     * @param src
     *            the source {@code this} represents.
     * @param injs
     *            the set of offsprings {@code this} injects.
     * @param c
     *            {@code this} initial concentration.
     */
    public Seed(final ISource src, final Set<IOffspring> injs,
            final IConcentration c) {
        this(src, injs);
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.concentration = new Concentration(c.toLong());
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ISeed#addOffspring(mok.model.IOffspring)
     */
    @Override
    public ISeed addOffspring(final IOffspring add) {
        if (add == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        // TODO No check for duplicates?
        this.injections.add(add);
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReactant#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IReactant decConcentration(final IConcentration c) {
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ISeed) {
            final ISeed o = (ISeed) obj;
            if (o.getSource().equals(this.source)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ISeed#getAtoms()
     */
    @Override
    public Set<IAtom> getAtoms() {
        final Set<IAtom> l = new LinkedHashSet<>();
        for (final IOffspring inj : this.injections) {
            l.add(inj.getAtom());
        }
        return l;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ISeed#getOffsprings()
     */
    @Override
    public Set<IOffspring> getOffsprings() {
        return this.injections;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ISeed#getSource()
     */
    @Override
    public ISource getSource() {
        return this.source;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.source.hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IProduct#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IProduct incConcentration(final IConcentration c) {
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ISeed#removeOffspring(mok.model.IOffspring)
     */
    @Override
    public ISeed removeOffspring(final IOffspring rem) {
        this.injections.remove(rem);
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuffer rep = new StringBuffer();
        rep.append(String.format("seed: [ %s, injections: { ", this.source));
        for (final IOffspring i : this.injections) {
            rep.append(i).append(" | ");
        }
        rep.delete(rep.length() - 2, rep.length()).append(" } ]");
        return rep.toString();
    }

}
