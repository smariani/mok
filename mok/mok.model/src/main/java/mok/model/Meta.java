/**
 * Created by Stefano Mariani on 19/giu/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.io.Serializable;
import java.util.Locale;

/**
 * <p>
 * Implementation class for {@link IMeta}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Meta implements IMeta, Serializable {

    private static final long serialVersionUID = 1L;
    private final String meta;

    /**
     * Empty constructor, builds an empty container for meta-information.
     */
    public Meta() {
        this.meta = "";
    }

    /**
     * Complete constructor, builds {@code this} with the given meta-information
     * as a {@link String}.
     *
     * @param m
     *            the string representation of {@code this}.
     */
    public Meta(final String m) {
        if (m == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.meta = m;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IMeta) {
            final IMeta m = (IMeta) obj;
            if (m.toString().equalsIgnoreCase(this.meta)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode() TODO Check correctness: is this correct
     * if equals ignores case??
     */
    @Override
    public int hashCode() {
        final String copy = new String(this.meta);
        return copy.toLowerCase(Locale.ROOT).hashCode();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.meta;
    }

    /* (non-Javadoc)
     * @see mok.model.IMeta#getMeta()
     */
    @Override
    public String getMeta() {
        return this.meta;
    }
}
