/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * <p>
 * Implementation class for {@link ISeedTemplate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class SeedTemplate implements ISeedTemplate {

    private IConcentration concentration;
    private final Set<IOffspringTemplate> injectionTemplates;
    private final ISourceTemplate sourceTemplate;

    /**
     * Minimal constructor, builds {@code this} with no
     * {@link IOffspringTemplate}s and minimal, minimal required
     * {@link IConcentration}.
     *
     * @param src
     *            the {@link ISourceTemplate} {@code this} considers.
     */
    public SeedTemplate(final ISourceTemplate src) {
        if (src == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.sourceTemplate = src;
        this.injectionTemplates = new LinkedHashSet<>();
        this.concentration = new Concentration();
    }

    /**
     * Minimal-concentration constructor, builds {@code this} with the given
     * {@link IOffspringTemplate} and minimal, minimal required
     * {@link IConcentration}.
     *
     * @param src
     *            the {@link ISourceTemplate} {@code this} considers.
     * @param injs
     *            the set of offspring templates {@code this} considers.
     */
    public SeedTemplate(final ISourceTemplate src,
            final Set<IOffspringTemplate> injs) {
        this(src);
        if (injs == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.injectionTemplates.addAll(injs);
    }

    /**
     * Complete constructor, builds {@code this} considering the given
     * {@link ISourceTemplate}, the given {@link IOffspringTemplate} and having
     * the given minimal required {@link IConcentration}.
     *
     * @param src
     *            the source template {@code this} considers.
     * @param injs
     *            the set of offspring templates {@code this} considers.
     * @param c
     *            {@code this} minimal required concentration.
     */
    public SeedTemplate(final ISourceTemplate src,
            final Set<IOffspringTemplate> injs, final IConcentration c) {
        this(src, injs);
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.concentration = new Concentration(c.toLong());
    }

    /* (non-Javadoc)
     * @see mok.model.IReactantTemplate#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IReactantTemplate decConcentration(final IConcentration decrement) {
        this.concentration.decConcentration(decrement.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentrationTemplate#checkMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatchingDegree checkMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentrationTemplate#doMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatch doMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ISeedTemplate#addOffspringTemplate(mok.model.IOffspringTemplate)
     */
    @Override
    public ISeedTemplate addOffspringTemplate(
            final IOffspringTemplate offspringTemplate) {
        if (offspringTemplate == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        // TODO No check for duplicates?
        this.injectionTemplates.add(offspringTemplate);
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.ISeedTemplate#getOffspringTemplates()
     */
    @Override
    public Set<IOffspringTemplate> getOffspringTemplates() {
        return this.injectionTemplates;
    }

    /* (non-Javadoc)
     * @see mok.model.ISeedTemplate#getSourceTemplate()
     */
    @Override
    public ISourceTemplate getSourceTemplate() {
        return this.sourceTemplate;
    }

    /* (non-Javadoc)
     * @see mok.model.ISeedTemplate#removeOffspringTemplate(mok.model.IOffspringTemplate)
     */
    @Override
    public ISeedTemplate removeOffspringTemplate(
            final IOffspringTemplate offspringTemplate) {
        this.injectionTemplates.remove(offspringTemplate);
        return this;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ISeedTemplate) {
            final ISeedTemplate o = (ISeedTemplate) obj;
            if (o.getSourceTemplate().equals(this.sourceTemplate)) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.sourceTemplate.hashCode();
        return res;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuffer rep = new StringBuffer();
        rep.append(String.format("seed template: [ %s, injections: { ",
                this.sourceTemplate));
        for (final IOffspringTemplate i : this.injectionTemplates) {
            rep.append(i).append(" | ");
        }
        rep.delete(rep.length() - 2, rep.length()).append(" } ]");
        return rep.toString();
    }

}
