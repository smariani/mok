/**
 * Created by Stefano Mariani on 06/giu/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.io.Serializable;
import java.util.Locale;

/**
 * <p>
 * Implementation class for {@link IContent}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Content implements IContent, Serializable {

    private static final long serialVersionUID = 1L;
    private final String content;

    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Only constructor, builds {@code this} with the given
     * {@link java.lang.String}.
     * </p>
     *
     * @param c
     *            the string representing {@code this}.
     */
    public Content(final String c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.content = c;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IContent) {
            final IContent c = (IContent) obj;
            if (c.toString().equalsIgnoreCase(this.content)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode() TODO Check correctness: is this correct
     * if equals ignores case??
     */
    @Override
    public int hashCode() {
        final String copy = new String(this.content);
        return copy.toLowerCase(Locale.ROOT).hashCode();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.content;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IContent#getContent()
     */
    @Override
    public String getContent() {
        return this.content;
    }
}
