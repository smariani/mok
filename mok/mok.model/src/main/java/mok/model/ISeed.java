/**
 * Created by Stefano Mariani on 27/mag/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.Set;

/**
 * <p>
 * Interface for MoK Seeds.
 * </p>
 * <p>
 * The {@link ISource} of information in MoK, seeds generate {@link IAtom}s by
 * participating {@link IInjection} reaction. Which atoms and how many of them
 * each seed generates, is defined by {@link IOffspring} abstraction.
 * </p>
 *
 * @see IReactant
 * @see IProduct
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface ISeed extends IReactant, IProduct {

    /**
     * Adds a {@link IOffspring} to the offsprings {@code this} generates.
     *
     * @param offspring
     *            the offsrping to add.
     * @return {@code this}.
     */
    ISeed addOffspring(IOffspring offspring);

    /**
     * Gets the {@link IAtom}s {@code this} can generate through its
     * {@link IOffspring}s.
     *
     * @return the set of atoms {@code this} can generate.
     */
    Set<IAtom> getAtoms();

    /**
     * Gets the {@link IOffspring}s {@code this} can generate.
     *
     * @return the set of offsprings {@code this} can generate.
     */
    Set<IOffspring> getOffsprings();

    /**
     * Gets the {@link ISource} of information {@code this} represents.
     *
     * @return the source of information {@code this} represents.
     */
    ISource getSource();

    /**
     * Removes a {@link IOffspring} from the set of offsprings {@code this} can
     * generate.
     *
     * @param offspring
     *            the offspring to remove.
     * @return {@code this}.
     */
    ISeed removeOffspring(IOffspring offspring);

}
