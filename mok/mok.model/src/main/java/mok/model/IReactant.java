/**
 * Created by Stefano Mariani on 20/set/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface representing MoK reactants, that is, any MoK abstraction which can
 * be consumed by at least one {@link IReaction}.
 * </p>
 *
 * @see IAtom
 * @see IEnzyme
 * @see IMolecule
 * @see ISeed
 * @see ITrace
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IReactant extends WithConcentration {

    /**
     * Decrements the {@link IConcentration} of {@code this}.
     *
     * @param decrement
     *            the concentration representing the decrement.
     * @return {@code this}.
     */
    IReactant decConcentration(IConcentration decrement);

}
