/**
 * Created by Stefano Mariani on 27/mag/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface for MoK Rate Expressions. TODO Define better rate expressions.
 * </p>
 * <p>
 * Rates dictates the scheduling probability and execution speed of
 * {@link IReaction}s, based on an approximation of well-known Gillespie's
 * stochastic algorithm for chemical solutions behaviour simulation (TODO
 * Explain better or cite paper).
 * </p>
 *
 * @see IReaction
 * @see AbstractReaction
 * @see IAggregation
 * @see IDecay
 * @see IDiffusion
 * @see IReinforcement
 * @see IInjection
 * @see IDeposit
 * @see IPerturbation
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IRate {

    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Decrements {@code this} of the given amount.
     * </p>
     *
     * @param decrement
     *            the decrement to apply.
     * @return {@code this}.
     */
    IRate decRate(double decrement);

    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Increments {@code this} of the given amount.
     * </p>
     *
     * @param increment
     *            the increment to apply.
     * @return {@code this}.
     */
    IRate incRate(double increment);

    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Maps {@code this} to a {@link long} value.
     * </p>
     *
     * @return the long value of {@code this}.
     */
    double toDouble();
}
