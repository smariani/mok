/**
 * Created by Stefano Mariani on 28/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/*
 * TODO Implement custom serialized form, implement clone() or factory constructor
 */
/**
 * <p>
 * Implementation class for {@link IEnzyme}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Enzyme implements IEnzyme {

    private final transient IConcentration concentration;
    private final IReactantTemplate reactantTemplate;
    private final ESpecies species;
    private final IConcentration strength;

    /**
     * Minimal constructor, builds {@code this} with minimal
     * {@link IConcentration} and strength.
     *
     * @param rt
     *            the {@link IReactantTemplate} {@code this} refers to.
     * @param s
     *            the {@link ESpecies} {@code this} belongs to.
     */
    public Enzyme(final IReactantTemplate rt, final ESpecies s) {
        if ((rt == null) || (s == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.reactantTemplate = rt;
        this.species = s;
        this.concentration = new Concentration();
        this.strength = new Concentration();
    }

    /**
     * Strength-specific constructor, builds {@code this} with given strength.
     *
     * @param rt
     *            the {@link IReactantTemplate} {@code this} refers to.
     * @param sp
     *            the {@link ESpecies} {@code this} belongs to.
     * @param st
     *            the strength of the reinforcement (in form of a
     *            {@link IConcentration}).
     */
    public Enzyme(final IReactantTemplate rt, final ESpecies sp,
            final IConcentration st) {
        if ((rt == null) || (sp == null) || (st == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.reactantTemplate = rt;
        this.species = sp;
        this.concentration = new Concentration();
        this.strength = st;
    }

    /**
     * Complete constructor, builds {@code this} with given strength and initial
     * {@link IConcentration}.
     *
     * @param rt
     *            the {@link IReactantTemplate} {@code this} refers to.
     * @param sp
     *            the {@link ESpecies} {@code this} belongs to.
     * @param st
     *            the strength of the reinforcement (in form of a
     *            {@link IConcentration}).
     * @param c
     *            the initial {@link IConcentration} of {@code this}.
     */
    public Enzyme(final IReactantTemplate rt, final ESpecies sp,
            final IConcentration st, final IConcentration c) {
        if ((rt == null) || (sp == null) || (st == null) || (c == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.reactantTemplate = rt;
        this.species = sp;
        this.concentration = c;
        this.strength = st;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReactant#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IEnzyme decConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IEnzyme#decStrength(mok.model.IConcentration)
     */
    @Override
    public IEnzyme decStrength(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.strength.decConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IEnzyme) {
            final IEnzyme e = (IEnzyme) obj;
            if (e.getReactantTemplate().equals(this.reactantTemplate)
                    && e.getSpecies().equals(this.species)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IEnzyme#getReactant()
     */
    @Override
    public IReactantTemplate getReactantTemplate() {
        return this.reactantTemplate;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IEnzyme#getSpecies()
     */
    @Override
    public ESpecies getSpecies() {
        return this.species;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IEnzyme#getStrength()
     */
    @Override
    public IConcentration getStrength() {
        return this.strength;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.reactantTemplate.hashCode();
        res = (31 * res) + this.species.hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IProduct#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IEnzyme incConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IEnzyme#incStrength(mok.model.IConcentration)
     */
    @Override
    public IEnzyme incStrength(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.strength.incConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format(
                "enzyme: [ reactant: %s, species: %s, strength: %s ](%s)",
                this.reactantTemplate, this.species, this.strength,
                this.concentration);
    }

}
