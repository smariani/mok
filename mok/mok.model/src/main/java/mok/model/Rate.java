/**
 * Created by Stefano Mariani on 20/set/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/*
 * TODO Consider thread-safety
 */
/**
 * <p>
 * Implementation class for {@link IRate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Rate implements IRate, Comparable<IRate> {

    private double rate;

    /**
     * Only constructor, builds {@code this} with the given {@link Double}
     * initial value.
     *
     * @param r
     *            the initial double value of {@code this}.
     */
    public Rate(final double r) {
        this.rate = r;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IRate#decRate(double)
     */
    @Override
    public IRate decRate(final double dec) {
        if (dec > this.rate) {
            this.rate = Double.MIN_VALUE;
        } else {
            this.rate -= dec;
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IRate) {
            final IRate o = (IRate) obj;
            return Double.compare(o.toDouble(), this.rate) == 0;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        final long l = Double.doubleToLongBits(this.rate);
        res = (31 * res) + (int) (l ^ (l >>> 32));
        return res;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IRate#incRate(double)
     */
    @Override
    public IRate incRate(final double inc) {
        this.rate += inc;
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IRate#toDouble()
     */
    @Override
    public double toDouble() {
        return this.rate;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.valueOf(this.rate);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(mok.model.IRate)
     */
    @Override
    public int compareTo(final IRate r) {
        if (r == null) {
            throw new NullPointerException();
        }
        if (this.rate < r.toDouble()) {
            return -1;
        } else if (this.rate > r.toDouble()) {
            return 1;
        } else {
            return 0;
        }
    }
}
