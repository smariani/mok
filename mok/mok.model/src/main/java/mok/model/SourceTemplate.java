/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.regex.Pattern;

/**
 * <p>
 * Implementation class for {@link ISourceTemplate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class SourceTemplate implements ISourceTemplate {

    private final Pattern descriptionTemplate;
    private final IURITemplate uriTemplate;

    /**
     * Minimal constructor, builds {@code this} given the {@link IURITemplate}
     * of the source of information considered.
     *
     * @param u
     *            the URI template of the source of information {@code this}
     *            considers.
     */
    public SourceTemplate(final IURITemplate u) {
        if (u == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.uriTemplate = u;
        this.descriptionTemplate = Pattern.compile("");
    }

    /**
     * Complete constructor, builds {@code this} with also a description
     * template of the considered source of information.
     *
     * @param u
     *            the {@link IURITemplate} of the source of information
     *            {@code this} considers.
     * @param d
     *            the description template of {@code this} in
     *            {@link java.util.regex.Pattern} format.
     */
    public SourceTemplate(final IURITemplate u, final Pattern d) {
        if ((u == null) || (d == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.uriTemplate = u;
        this.descriptionTemplate = d;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#checkMatching(java.lang.Object)
     */
    @Override
    public IMatchingDegree checkMatching(final ISourceTemplate t) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#doMatching(java.lang.Object)
     */
    @Override
    public IMatch doMatching(final ISourceTemplate t) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ISourceTemplate#getDescriptionTemplate()
     */
    @Override
    public Pattern getDescriptionTemplate() {
        return this.descriptionTemplate;
    }

    /* (non-Javadoc)
     * @see mok.model.ISourceTemplate#getURITemplate()
     */
    @Override
    public IURITemplate getURITemplate() {
        return this.uriTemplate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ISourceTemplate) {
            final ISourceTemplate o = (ISourceTemplate) obj;
            if (o.getURITemplate().equals(this.uriTemplate)
                    && o.getDescriptionTemplate().pattern()
                            .equals(this.descriptionTemplate.pattern())) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.uriTemplate.hashCode();
        res = (31 * res) + this.descriptionTemplate.pattern().hashCode();
        return res;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuffer rep = new StringBuffer();
        rep.append(String.format("source template: %s", this.uriTemplate));
        if (!this.descriptionTemplate.pattern().isEmpty()) {
            rep.append(
                    String.format(", %s", this.descriptionTemplate.pattern()));
        }
        return rep.toString();
    }

}
