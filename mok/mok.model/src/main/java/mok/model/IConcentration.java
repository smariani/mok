/**
 * Created by Stefano Mariani on 27/mag/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface for MoK Concentration value.
 * </p>
 * <p>
 * Concentration in MoK is a value resembling the relative relevance of an
 * abstraction with respect to other abstractions (with a concentration) living
 * in the same compartment.
 * </p>
 *
 * @see WithConcentration
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IConcentration {

    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Decrements {@code this}.
     * </p>
     *
     * @param decrement
     *            the decrement to apply.
     * @return {@code this}.
     */
    IConcentration decConcentration(long decrement);

    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Increments {@code this}.
     * </p>
     *
     * @param increment
     *            the increment to apply.
     * @return {@code this}.
     */
    IConcentration incConcentration(long increment);

    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Maps {@code this} to a {@link java.lang.Long} value.
     * </p>
     *
     * @return the long value of {@code this}.
     */
    long toLong();
}
