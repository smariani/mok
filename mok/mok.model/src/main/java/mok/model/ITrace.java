/**
 * Created by Stefano Mariani on 20/set/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface for MoK Traces.
 * </p>
 * <p>
 * The reification of users' actions' side-effects, traces are released by
 * {@link IEnzyme}s through {@link IDeposit} reaction, to resemble long-lasting
 * effects of users' epistemic actions.
 * </p>
 * <p>
 * Traces refer to the depositing {@link IEnzyme}, have a {@link IConcentration}
 * and carry a {@link IPerturbationAction}, eventually applied to
 * {@link IMolecule} the enzyme refers to.
 * </p>
 *
 * @see IReactant
 * @see IProduct
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface ITrace extends IReactant, IProduct {

    /*
     * TODO Template or just Enzyme?
     */
    /**
     * Gets the {@link IEnzymeTemplate} {@code this} has been deposited by.
     *
     * @return the enzyme template.
     */
    IEnzymeTemplate getEnzymeTemplate();

    /**
     * Gets the {@link IPerturbationAction} {@code this} eventually applies.
     *
     * @return the perturbation action.
     */
    IPerturbationAction getPerturbationAction();

}
