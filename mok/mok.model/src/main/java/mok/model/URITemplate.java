/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.regex.Pattern;

/**
 * <p>
 * Implementation class for {@link IURITemplate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class URITemplate implements IURITemplate {

    private final Pattern uriTemplate;

    /**
     * Only constructor, builds {@code this} given the
     * {@link java.util.regex.Pattern} to consider.
     *
     * @param p
     *            the pattern {@code this} considers.
     *
     */
    public URITemplate(final Pattern p) {
        this.uriTemplate = p;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#checkMatching(java.lang.Object)
     */
    @Override
    public IMatchingDegree checkMatching(final IURITemplate t) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#doMatching(java.lang.Object)
     */
    @Override
    public IMatch doMatching(final IURITemplate t) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IURITemplate#getURITemplate()
     */
    @Override
    public Pattern getURITemplate() {
        return this.uriTemplate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IURITemplate) {
            final IURITemplate ct = (IURITemplate) obj;
            if (ct.getURITemplate().pattern()
                    .equals(this.uriTemplate.pattern())) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.uriTemplate.pattern().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.uriTemplate.pattern();
    }

}
