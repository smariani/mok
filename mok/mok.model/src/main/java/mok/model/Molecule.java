/**
 * Created by Stefano Mariani on 20/set/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.LinkedHashSet;
import java.util.Set;

/*
 * TODO Implement custom serialized form, implement clone() or factory constructor
 */
/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Molecule implements IMolecule {

    private final Set<IAtom> atoms;
    private final transient IConcentration concentration;

    /**
     * Minimal constructor, builds {@code this} aggregating the two given
     * {@link IAtom}s.
     *
     * @param a1
     *            the atom to aggregate.
     * @param a2
     *            the other atom to aggregate.
     */
    public Molecule(final IAtom a1, final IAtom a2) {
        if ((a1 == null) || (a2 == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        if (a1.equals(a2)) {
            throw new IllegalArgumentException(
                    "Molecule made of two (or more) identical atoms is meaningless!");
        }
        this.atoms = new LinkedHashSet<>();
        this.atoms.add(a1);
        this.atoms.add(a2);
        this.concentration = new Concentration();
    }

    /**
     * Arbitrary constructor, builds {@code this} aggregating the given set of
     * {@link IAtom}s.
     *
     * @param as
     *            the set of {@link IAtom}s {@code this} aggregates.
     */
    public Molecule(final Set<IAtom> as) {
        if (as == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.atoms = new LinkedHashSet<>();
        for (final IAtom atom : as) {
            // TODO Clone or factory! Reset concentration?
            this.atoms.add(atom);
        }
        this.concentration = new Concentration();
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IMolecule#addAtom(mok.model.IAtom)
     */
    @Override
    public IMolecule addAtom(final IAtom a) {
        if (a == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        if (!this.atoms.contains(a)) {
            // TODO Clone or factory! Reset concentration?
            this.atoms.add(a);
        } else {
            this.atoms.parallelStream().filter(atom -> atom.equals(a)).forEach(
                    atom -> atom.incConcentration(new Concentration()));
            this.concentration.incConcentration(Concentration.MIN_CONC);
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IMolecule#addAtoms(java.util.Set)
     */
    @Override
    public IMolecule addAtoms(final Set<IAtom> as) {
        if (as == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        for (final IAtom atom : as) {
            // TODO Clone or factory! Reset concentration?
            this.addAtom(atom);
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReactant#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IMolecule decConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        boolean result = true;
        if (obj instanceof IMolecule) {
            final IMolecule o = (IMolecule) obj;
            if (o.getAtoms().size() == this.atoms.size()) {
                result = !this.atoms.parallelStream()
                        .anyMatch(atom -> !o.getAtoms().contains(atom));
            } else {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IMolecule#getAtoms()
     */
    @Override
    public Set<IAtom> getAtoms() {
        return this.atoms;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.atoms.hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IProduct#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IMolecule incConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IMolecule#removeAtom(mok.model.IMolecule)
     */
    @Override
    public IMolecule removeAtom(final IAtom a) {
        if (this.atoms.contains(a)) {
            // Cannot use stream due to removal
            for (final IAtom atom : this.atoms) {
                if (atom.equals(a)) {
                    final IConcentration c = atom.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.atoms.remove(atom);
                    } else {
                        atom.decConcentration(new Concentration());
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuffer rep = new StringBuffer();
        rep.append("molecule: { ");
        for (final IAtom a : this.atoms) {
            rep.append(a).append(" | ");
        }
        rep.delete(rep.length() - 2, rep.length())
                .append(String.format(" }(%s)", this.concentration));
        return rep.toString();
    }
}
