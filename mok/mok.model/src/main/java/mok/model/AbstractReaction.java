/**
 * Created by Stefano Mariani on 25/giu/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * <p>
 * Base class for {@link IReaction}.
 * </p>
 *
 * @see IDecay
 * @see IInjection
 * @see IAggregation
 * @see IDiffusion
 * @see IReinforcement
 * @see IPerturbation
 * @see IDeposit
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public abstract class AbstractReaction implements IReaction, Comparable<IRate> {

    private final Set<IProductTemplate> productTemplates;
    private final IRate rate;
    private final Set<IReactantTemplate> reactantTemplates;

    /**
     * Only constructor, builds {@code this} with the given {@link IRate} but no
     * {@link IProductTemplate}s nor {@link IReactantTemplate}s.
     *
     * @param r
     *            the initial rate of {@code this}.
     */
    public AbstractReaction(final IRate r) {
        this.productTemplates = new LinkedHashSet<>();
        this.rate = new Rate(r.toDouble());
        this.reactantTemplates = new LinkedHashSet<>();
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReaction#addProductTemplate(mok.model.IProductTemplate)
     */
    @Override
    public final IReaction addProductTemplate(final IProductTemplate pt) {
        if (pt == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        // TODO Clone or factory!
        if (!this.productTemplates.contains(pt)) {
            this.productTemplates.add(pt);
        } else {
            this.productTemplates.parallelStream()
                    .filter(product -> product.equals(pt))
                    .forEach(product -> product
                            .incConcentration(new Concentration()));
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReaction#addReactantTemplate(mok.model.IReactantTemplate)
     */
    @Override
    public final IReaction addReactantTemplate(final IReactantTemplate rt) {
        if (rt == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        // TODO Clone or factory!
        if (!this.reactantTemplates.contains(rt)) {
            this.reactantTemplates.add(rt);
        } else {
            this.reactantTemplates.parallelStream()
                    .filter(reactant -> reactant.equals(rt))
                    .forEach(reactant -> reactant.getConcentration()
                            .incConcentration(Concentration.MIN_CONC));
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReaction#decRate(IRate)
     */
    @Override
    public final IReaction decRate(final IRate r) {
        this.rate.decRate(r.toDouble());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReaction#getProductTemplates()
     */
    @Override
    public final Set<IProductTemplate> getProductTemplates() {
        return this.productTemplates;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReaction#getRate()
     */
    @Override
    public final IRate getRate() {
        return this.rate;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReaction#getReactantTemplates()
     */
    @Override
    public final Set<IReactantTemplate> getReactantTemplates() {
        return this.reactantTemplates;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReaction#incRate(IRate)
     */
    @Override
    public final IReaction incRate(final IRate r) {
        this.rate.incRate(r.toDouble());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see
     * mok.model.IReaction#removeProductTemplate(mok.model.IProductTemplate)
     */
    @Override
    public final IReaction removeProductTemplate(final IProductTemplate pt) {
        if (this.productTemplates.contains(pt)) {
            // Can't use streams due to removal
            for (final IProductTemplate product : this.productTemplates) {
                if (product.equals(pt)) {
                    final IConcentration c = product.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.productTemplates.remove(product);
                    } else {
                        product.getConcentration()
                                .decConcentration(Concentration.MIN_CONC);
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see
     * mok.model.IReaction#removeReactantTemplate(mok.model.IReactantTemplate)
     */
    @Override
    public final IReaction removeReactantTemplate(final IReactantTemplate rt) {
        if (this.reactantTemplates.contains(rt)) {
            // Can't use streams due to removal
            for (final IReactantTemplate reactant : this.reactantTemplates) {
                if (reactant.equals(rt)) {
                    final IConcentration c = reactant.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.reactantTemplates.remove(reactant);
                    } else {
                        reactant.decConcentration(new Concentration());
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public final String toString() {
        final StringBuffer buff = new StringBuffer();
        buff.append("reaction [ ").append(this.rate);
        for (final IReactantTemplate rt : this.reactantTemplates) {
            buff.append(rt).append(" + ");
        }
        buff.delete(buff.length() - 2, buff.length())
                .append(String.format("--@%f--> ", this.rate.toDouble()));
        for (final IProductTemplate pt : this.productTemplates) {
            buff.append(pt).append(" + ");
        }
        buff.delete(buff.length() - 2, buff.length()).append("]");
        return buff.toString();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(mok.model.IRate)
     */
    @Override
    public final int compareTo(final IRate r) {
        return ((Rate) this.rate).compareTo(r);
    }
}
