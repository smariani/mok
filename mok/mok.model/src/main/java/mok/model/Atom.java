/**
 * Created by Stefano Mariani on 27/mag/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/*
 * TODO Implement custom serialized form, implement clone() or factory constructor
 */
/**
 * <p>
 * Implementation class for {@link IAtom}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 */
public final class Atom implements IAtom {

    private final transient IConcentration concentration;
    private final IContent content;
    private final IMeta meta;
    private final transient ISeed seed;

    /**
     * Minimal constructor, builds {@code this} with no {@link IMeta} and
     * minimal {@link IConcentration}.
     *
     * @param s
     *            the {@link ISeed} who generated {@code this}.
     * @param c
     *            the {@link IContent} of {@code this}.
     */
    public Atom(final ISeed s, final IContent c) {
        if ((s == null) || (c == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.seed = s;
        this.content = c;
        this.meta = new Meta();
        this.concentration = new Concentration();
    }

    /**
     * Semantic constructor, builds {@code this} decorated with {@link IMeta} to
     * ease semantic interpretation.
     *
     * @param s
     *            the {@link ISeed} who generated {@code this}.
     * @param c
     *            the {@link IContent} of {@code this}.
     * @param m
     *            the meta-information related to the content of {@code this}.
     */
    public Atom(final ISeed s, final IContent c, final IMeta m) {
        if ((s == null) || (c == null) || (m == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.seed = s;
        this.content = c;
        this.meta = m;
        this.concentration = new Concentration();
    }

    /**
     * Complete constructor, builds {@code this} decorated with {@link IMeta}
     * and having the given initial {@link IConcentration}.
     *
     * @param s
     *            the {@link ISeed} who generated {@code this}.
     * @param cont
     *            the {@link IContent} of {@code this}.
     * @param m
     *            the meta-information related to the content of {@code this}.
     * @param conc
     *            the initial concentration of {@code this}.
     */
    public Atom(final ISeed s, final IContent cont, final IMeta m,
            final IConcentration conc) {
        if ((s == null) || (cont == null) || (m == null) || (conc == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.seed = s;
        this.content = cont;
        this.meta = m;
        this.concentration = conc;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReactant#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IAtom decConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IAtom) {
            final IAtom a = (IAtom) obj;
            if (a.getSeed().equals(this.seed)
                    && a.getContent().equals(this.content)
                    && a.getMeta().equals(this.meta)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IAtom#getContent()
     */
    @Override
    public IContent getContent() {
        return this.content;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IAtom#getMeta()
     */
    @Override
    public IMeta getMeta() {
        return this.meta;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IAtom#getSeed()
     */
    @Override
    public ISeed getSeed() {
        return this.seed;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.seed.hashCode();
        res = (31 * res) + this.content.hashCode();
        res = (31 * res) + this.meta.hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IProduct#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IAtom incConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("atom: [ seed: %s, content: %s, meta: %s ](%s)",
                this.seed.getSource(), this.content, this.meta,
                this.concentration);
    }

}
