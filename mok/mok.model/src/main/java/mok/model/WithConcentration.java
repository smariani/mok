/**
 * Created by Stefano Mariani on 01/lug/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface representing any MoK abstraction equipped with a concentration
 * value.
 * </p>
 *
 * @see mok.model.IReactant
 * @see mok.model.IProduct
 * @see mok.model.IReactantTemplate
 * @see mok.model.IProductTemplate
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 * @author (contributor) Mirko Viroli
 *
 */
public interface WithConcentration {

    /**
     * Gets the {@link mok.model.IConcentration} of the implementing MoK
     * abstraction.
     *
     * @return the concentration of the implementing MoK abstraction.
     */
    IConcentration getConcentration();

}
