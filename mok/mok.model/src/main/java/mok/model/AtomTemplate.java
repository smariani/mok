/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Implementation class for {@link IAtomTemplate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class AtomTemplate implements IAtomTemplate {

    private final IConcentration concentration;
    private final IContentTemplate contentTemplate;
    private final IMetaTemplate metaTemplate;
    private final ISeedTemplate seedTemplate;

    /**
     * Minimal constructor, builds {@code this} with no {@link IMetaTemplate}
     * and minimal, minimal required {@link IConcentration}.
     *
     * @param s
     *            the {@link ISeedTemplate} of {@code this}.
     * @param c
     *            the {@link IContentTemplate} of {@code this}.
     */
    public AtomTemplate(final ISeedTemplate s, final IContentTemplate c) {
        if ((s == null) || (c == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.seedTemplate = s;
        this.contentTemplate = c;
        this.metaTemplate = new MetaTemplate();
        this.concentration = new Concentration();
    }

    /**
     * Semantic constructor, builds {@code this} decorated with
     * {@link IMetaTemplate}.
     *
     * @param s
     *            the {@link ISeedTemplate} of {@code this}.
     * @param c
     *            the {@link IContentTemplate} of {@code this}.
     * @param m
     *            the meta-information template of {@code this}.
     */
    public AtomTemplate(final ISeedTemplate s, final IContentTemplate c,
            final IMetaTemplate m) {
        if ((s == null) || (c == null) || (m == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.seedTemplate = s;
        this.contentTemplate = c;
        this.metaTemplate = m;
        this.concentration = new Concentration();
    }

    /**
     * Complete constructor, builds {@code this} decorated with
     * {@link IMetaTemplate} and having the given minimal required
     * {@link IConcentration}.
     *
     * @param s
     *            the {@link ISeedTemplate} of {@code this}.
     * @param cont
     *            the {@link IContentTemplate} of {@code this}.
     * @param m
     *            the meta-information template of {@code this}.
     * @param conc
     *            the minimal required concentration.
     */
    public AtomTemplate(final ISeedTemplate s, final IContentTemplate cont,
            final IMetaTemplate m, final IConcentration conc) {
        if ((s == null) || (cont == null) || (m == null) || (conc == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.seedTemplate = s;
        this.contentTemplate = cont;
        this.metaTemplate = m;
        this.concentration = conc;
    }

    /* (non-Javadoc)
     * @see mok.model.IReactantTemplate#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IReactantTemplate decConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#checkMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatchingDegree checkMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#doMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatch doMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IProductTemplate#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IProductTemplate incConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.IAtomTemplate#getContentTemplate()
     */
    @Override
    public IContentTemplate getContentTemplate() {
        return this.contentTemplate;
    }

    /* (non-Javadoc)
     * @see mok.model.IAtomTemplate#getMetaTemplate()
     */
    @Override
    public IMetaTemplate getMetaTemplate() {
        return this.metaTemplate;
    }

    /* (non-Javadoc)
     * @see mok.model.IAtomTemplate#getSeedTemplate()
     */
    @Override
    public ISeedTemplate getSeedTemplate() {
        return this.seedTemplate;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IAtomTemplate) {
            final IAtomTemplate a = (IAtomTemplate) obj;
            if (a.getSeedTemplate().equals(this.seedTemplate)
                    && a.getContentTemplate().equals(this.contentTemplate)
                    && a.getMetaTemplate().equals(this.metaTemplate)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.seedTemplate.hashCode();
        res = (31 * res) + this.contentTemplate.hashCode();
        res = (31 * res) + this.metaTemplate.hashCode();
        return res;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format(
                "atom template: [ seed: %s, content: %s, meta: %s ](%s)",
                this.seedTemplate.getSourceTemplate(), this.contentTemplate,
                this.metaTemplate, this.concentration);
    }

}
