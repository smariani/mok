/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.regex.Pattern;

/**
 * <p>
 * Base class for {@link IPerturbationActionTemplate}.
 * </p>
 *
 * @see TODO Actual perturbation actions templates
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public abstract class AbstractPerturbationActionTemplate
        implements IPerturbationActionTemplate {

    private final Pattern descriptionTemplate;
    private final IReactantTemplate reactantTemplate;

    /**
     * Only constructor, builds {@code this} with the given
     * {@link IReactantTemplate} and description template (as a
     * {@link java.util.regex.Pattern}).
     *
     * @param rt
     *            the reactant template {@code this} considers to.
     * @param p
     *            the description template {@code this} considers.
     */
    public AbstractPerturbationActionTemplate(final IReactantTemplate rt,
            final Pattern p) {
        if ((rt == null) || (p == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.reactantTemplate = rt;
        this.descriptionTemplate = p;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#checkMatching(java.lang.Object)
     */
    @Override
    public final IMatchingDegree checkMatching(final IPerturbationActionTemplate t) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#doMatching(java.lang.Object)
     */
    @Override
    public final IMatch doMatching(final IPerturbationActionTemplate t) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IPerturbationActionTemplate#getDescriptionTemplate()
     */
    @Override
    public final Pattern getDescriptionTemplate() {
        return this.getDescriptionTemplate();
    }

    /* (non-Javadoc)
     * @see mok.model.IPerturbationActionTemplate#getReactantTemplate()
     */
    @Override
    public final IReactantTemplate getReactantTemplate() {
        return this.reactantTemplate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IPerturbationActionTemplate) {
            final IPerturbationActionTemplate pat = (IPerturbationActionTemplate) obj;
            if (pat.getReactantTemplate().equals(this.reactantTemplate)
                    && pat.getDescriptionTemplate().pattern()
                            .equals(this.descriptionTemplate.pattern())) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public final int hashCode() {
        int res = 17;
        res = (31 * res) + this.reactantTemplate.hashCode();
        res = (31 * res) + this.descriptionTemplate.pattern().hashCode();
        return res;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public final String toString() {
        return String.format("perturbation template: %s -> ( reactant: %s )",
                this.descriptionTemplate.pattern(), this.reactantTemplate);
    }

}
