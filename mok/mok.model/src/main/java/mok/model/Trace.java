/**
 * Created by Stefano Mariani on 28/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/*
 * TODO Implement custom serialized form, implement clone() or factory constructor
 */
/**
 * <p>
 * Implementation class for {@link ITrace}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Trace implements ITrace {

    private final IConcentration concentration;
    private final IEnzymeTemplate enzyme;
    private final IPerturbationAction perturbation;

    // private final IReactant reactant;

    /**
     * Minimal constructor, builds {@code this} with the given
     * {@link IEnzymeTemplate} and the given {@link IPerturbationAction}.
     *
     * @param e
     *            the enzyme template who deposited {@code this}.
     * @param p
     *            the perturbation action {@code this} eventually applies.
     */
    public Trace(final IEnzymeTemplate e, final IPerturbationAction p) {
        if ((e == null) || (p == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.enzyme = e;
        this.perturbation = p;
        this.concentration = new Concentration();
    }

    /**
     * Complete constructor, builds {@code this} also with the given initial
     * {@link IConcentration}.
     *
     * @param e
     *            the {@link IEnzymeTemplate} who deposited {@code this}.
     * @param p
     *            the {@link IPerturbationAction} {@code this} eventually
     *            applies.
     * @param c
     *            the initial concentration of {@code this}.
     */
    public Trace(final IEnzymeTemplate e, final IPerturbationAction p,
            final IConcentration c) {
        if ((e == null) || (p == null) || (c == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.enzyme = e;
        this.perturbation = p;
        this.concentration = c;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReactant#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IReactant decConcentration(final IConcentration c) {
        // TODO Null check
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ITrace) {
            final ITrace o = (ITrace) obj;
            if (o.getEnzymeTemplate().equals(this.enzyme)
                    && o.getPerturbationAction().equals(this.perturbation)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ITrace#getEnzymeTemplate()
     */
    @Override
    public IEnzymeTemplate getEnzymeTemplate() {
        return this.enzyme;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ITrace#getPerturbationAction()
     */
    @Override
    public IPerturbationAction getPerturbationAction() {
        return this.perturbation;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.enzyme.hashCode();
        res = (31 * res) + this.perturbation.hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IProduct#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IProduct incConcentration(final IConcentration c) {
        // TODO Null check
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("trace: [ enzyme: %s, perturbation: %s ](%s)",
                this.enzyme, this.perturbation.getDescription(),
                this.concentration);
    }

}
