/**
 * Created by Stefano Mariani on 26/giu/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface for {@link IAtom} templates.
 * </p>
 *
 * @see IReactantTemplate
 * @see IProductTemplate
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IAtomTemplate extends IReactantTemplate, IProductTemplate {

    /**
     * Gets the {@link IContentTemplate} of {@code this}.
     *
     * @return the content template of {@code this}.
     */
    IContentTemplate getContentTemplate();

    /**
     * Gets the {@link IMetaTemplate} of {@code this}.
     *
     * @return the meta-information template of {@code this}.
     */
    IMetaTemplate getMetaTemplate();

    /**
     * Gets the {@link ISeedTemplate} of {@code this}.
     *
     * @return the seed template of {@code this}.
     */
    ISeedTemplate getSeedTemplate();

}
