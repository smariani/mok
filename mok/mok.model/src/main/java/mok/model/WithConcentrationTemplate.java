/**
 * Created by Stefano Mariani on 07/lug/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface representing any MoK template, that is, templates of any MoK
 * abstraction which can be consumed or produced by at least one MoK reaction.
 * </p>
 *
 * @see IProductTemplate
 * @see IReactantTemplate
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface WithConcentrationTemplate {

    /**
     * Checks whether the given MoK abstraction complies to this template. TODO
     * Explain better how matching is done.
     *
     * @param wc
     *            the MoK abstraction to check.
     * @return the {@link mok.model.IMatchingDegree} measuring similarity
     *         between this template and the given MoK abstraction.
     */
    IMatchingDegree checkMatching(WithConcentration wc);

    /**
     * Performs matching between this template and a given MoK abstraction.
     *
     * @param wc
     *            the MoK abstraction to match.
     * @return the {@link mok.model.IMatch} object storing matching results.
     *         TODO Explain better what are the effect of matching.
     */
    IMatch doMatching(WithConcentration wc);

}
