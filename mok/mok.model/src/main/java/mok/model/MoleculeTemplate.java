/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * <p>
 * Implementation class for {@link IMoleculeTemplate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class MoleculeTemplate implements IMoleculeTemplate {

    private final Set<IAtomTemplate> atomTemplates;
    private final transient IConcentration concentration;

    /**
     * Minimal constructor, builds {@code this} with the two given
     * {@link IAtomTemplate}s.
     *
     * @param a1
     *            the atom template to consider.
     * @param a2
     *            the other atom template to consider.
     */
    public MoleculeTemplate(final IAtomTemplate a1, final IAtomTemplate a2) {
        if ((a1 == null) || (a2 == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        if (a1.equals(a2)) {
            throw new IllegalArgumentException(
                    "Molecule template made of two (or more) identical atom templates is meaningless!");
        }
        this.atomTemplates = new LinkedHashSet<>();
        this.atomTemplates.add(a1);
        this.atomTemplates.add(a2);
        this.concentration = new Concentration();
    }

    /**
     * Arbitrary constructor, builds {@code this} considering the given set of
     * {@link IAtomTemplate}s.
     *
     * @param as
     *            the set of {@link IAtomTemplate}s {@code this} considers.
     */
    public MoleculeTemplate(final Set<IAtomTemplate> as) {
        if (as == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.atomTemplates = new LinkedHashSet<>();
        for (final IAtomTemplate atomTemplate : as) {
            // TODO Clone or factory! Reset concentration?
            this.atomTemplates.add(atomTemplate);
        }
        this.concentration = new Concentration();
    }

    /* (non-Javadoc)
     * @see mok.model.IMoleculeTemplate#addAtomTemplate(mok.model.IAtomTemplate)
     */
    @Override
    public IMoleculeTemplate addAtomTemplate(final IAtomTemplate at) {
        if (at == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        if (!this.atomTemplates.contains(at)) {
            // TODO Clone or factory! Reset concentration?
            this.atomTemplates.add(at);
        } else {
            this.atomTemplates.parallelStream()
                    .filter(atomT -> atomT.equals(at)).forEach(atomT -> atomT
                            .incConcentration(new Concentration()));
            this.concentration.incConcentration(Concentration.MIN_CONC);
        }
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.IMoleculeTemplate#addAtomTemplates(java.util.Set)
     */
    @Override
    public IMoleculeTemplate addAtomTemplates(final Set<IAtomTemplate> as) {
        if (as == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        for (final IAtomTemplate atomT : as) {
            // TODO Clone or factory! Reset concentration?
            this.addAtomTemplate(atomT);
        }
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.IMoleculeTemplate#getAtomTemplates()
     */
    @Override
    public Set<IAtomTemplate> getAtomTemplates() {
        return this.atomTemplates;
    }

    /* (non-Javadoc)
     * @see mok.model.IMoleculeTemplate#removeAtomTemplate(mok.model.IAtomTemplate)
     */
    @Override
    public IMoleculeTemplate removeAtomTemplate(final IAtomTemplate ats) {
        if (this.atomTemplates.contains(ats)) {
            // Cannot use stream due to removal
            for (final IAtomTemplate atomT : this.atomTemplates) {
                if (atomT.equals(ats)) {
                    final IConcentration c = atomT.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.atomTemplates.remove(atomT);
                    } else {
                        atomT.decConcentration(new Concentration());
                    }
                }
            }
        }
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.IMoleculeTemplate#checkMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatchingDegree checkMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IMoleculeTemplate#doMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatch doMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IReactantTemplate#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IReactantTemplate decConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /* (non-Javadoc)
     * @see mok.model.IProductTemplate#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IProductTemplate incConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        boolean result = true;
        if (obj instanceof IMoleculeTemplate) {
            final IMoleculeTemplate o = (IMoleculeTemplate) obj;
            if (o.getAtomTemplates().size() == this.atomTemplates.size()) {
                result = !this.atomTemplates.parallelStream()
                        .anyMatch(atom -> !o.getAtomTemplates().contains(atom));
            } else {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.atomTemplates.hashCode();
        return res;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuffer rep = new StringBuffer();
        rep.append("molecule template: { ");
        for (final IAtomTemplate a : this.atomTemplates) {
            rep.append(a).append(" | ");
        }
        rep.delete(rep.length() - 2, rep.length())
                .append(String.format(" }(%s)", this.concentration));
        return rep.toString();
    }

}
