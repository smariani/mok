/**
 * Created by Stefano Mariani on 26/giu/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.Set;

/**
 * <p>
 * Interface for {@link ISeed} templates.
 * </p>
 *
 * @see IReactantTemplate
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface ISeedTemplate extends IReactantTemplate {

    /**
     * Adds an {@link IOffspringTemplate} to {@code this}.
     *
     * @param offspringTemplate
     *            the offspring template to add to {@code this}.
     * @return {@code this}.
     */
    ISeedTemplate addOffspringTemplate(IOffspringTemplate offspringTemplate);

    /**
     * Gets the set of {@link IOffspringTemplate}s from {@code this}.
     *
     * @return the set of offspring templates {@code this} includes.
     */
    Set<IOffspringTemplate> getOffspringTemplates();

    /**
     * Gets the {@link ISourceTemplate} from {@code this}.
     *
     * @return the source template {@code this} includes.
     */
    ISourceTemplate getSourceTemplate();

    /**
     * Removes an {@link IOffspringTemplate} from {@code this}.
     *
     * @param offspringTemplate
     *            the offspring template to remove from {@code this}.
     * @return {@code this}.
     */
    ISeedTemplate removeOffspringTemplate(IOffspringTemplate offspringTemplate);

}
