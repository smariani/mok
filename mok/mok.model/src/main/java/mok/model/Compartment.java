/**
 * Created by Stefano Mariani on 05/giu/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * <p>
 * Implementation class for {@link ICompartment}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Compartment implements ICompartment {

    private final Set<IAtom> atoms;
    private final Set<IEnzyme> enzymes;
    private final Set<IMolecule> molecules;
    private final Set<ISeed> seeds;
    private final Set<ITrace> traces;

    /**
     * Only constructor, builds an empty {@code this}.
     */
    public Compartment() {
        this.atoms = new LinkedHashSet<>();
        this.enzymes = new LinkedHashSet<>();
        this.molecules = new LinkedHashSet<>();
        this.seeds = new LinkedHashSet<>();
        this.traces = new LinkedHashSet<>();
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#addAtom(mok.model.IAtom)
     */
    @Override
    public ICompartment addAtom(final IAtom a) {
        if (a == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        if (!this.atoms.contains(a)) {
            // TODO Clone or factory!
            this.atoms.add(a);
        } else {
            this.atoms.parallelStream().filter(atom -> atom.equals(a)).forEach(
                    atom -> atom.incConcentration(new Concentration()));
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#addEnzyme(mok.model.IEnzyme)
     */
    @Override
    public ICompartment addEnzyme(final IEnzyme e) {
        if (e == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        if (!this.enzymes.contains(e)) {
            // TODO Clone or factory!
            this.enzymes.add(e);
        } else {
            this.enzymes.parallelStream().filter(enzyme -> enzyme.equals(e))
                    .forEach(enzyme -> enzyme
                            .incConcentration(new Concentration()));
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#addMolecule(mok.model.IMolecule)
     */
    @Override
    public ICompartment addMolecule(final IMolecule m) {
        if (m == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        if (!this.molecules.contains(m)) {
            // TODO Clone or factory!
            this.molecules.add(m);
        } else {
            this.molecules.parallelStream()
                    .filter(molecule -> molecule.equals(m))
                    .forEach(molecule -> molecule
                            .incConcentration(new Concentration()));
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#addSeed(mok.model.ISeed)
     */
    @Override
    public ICompartment addSeed(final ISeed s) {
        if (s == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        if (!this.seeds.contains(s)) {
            // TODO Clone or factory!
            this.seeds.add(s);
        } else {
            for (final ISeed seed : this.seeds) {
                if (seed.equals(s)) {
                    for (final IOffspring injection : s.getOffsprings()) {
                        if (!seed.getOffsprings().contains(injection)) {
                            seed.addOffspring(injection);
                        }
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#addTrace(mok.model.ITrace)
     */
    @Override
    public ICompartment addTrace(final ITrace t) {
        if (t == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        if (!this.traces.contains(t)) {
            // TODO Clone or factory!
            this.traces.add(t);
        } else {
            this.traces.parallelStream().filter(trace -> trace.equals(t))
                    .forEach(trace -> trace
                            .incConcentration(new Concentration()));
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#getAtoms()
     */
    @Override
    public Set<IAtom> getAtoms() {
        return this.atoms;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#getEnzymes()
     */
    @Override
    public Set<IEnzyme> getEnzymes() {
        return this.enzymes;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#getMolecules()
     */
    @Override
    public Set<IMolecule> getMolecules() {
        return this.molecules;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#getSeeds()
     */
    @Override
    public Set<ISeed> getSeeds() {
        return this.seeds;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#getTraces()
     */
    @Override
    public Set<ITrace> getTraces() {
        return this.traces;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#removeAtom(mok.model.IAtom)
     */
    @Override
    public ICompartment removeAtom(final IAtom a) {
        if (this.atoms.contains(a)) {
            // Can't use streams due to removal
            for (final IAtom atom : this.atoms) {
                if (atom.equals(a)) {
                    final IConcentration c = atom.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.atoms.remove(atom);
                    } else {
                        atom.getConcentration()
                                .decConcentration(Concentration.MIN_CONC);
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#removeEnzyme(mok.model.IEnzyme)
     */
    @Override
    public ICompartment removeEnzyme(final IEnzyme e) {
        if (this.enzymes.contains(e)) {
            // Can't use streams due to removal
            for (final IEnzyme enzyme : this.enzymes) {
                if (enzyme.equals(e)) {
                    final IConcentration c = enzyme.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.enzymes.remove(enzyme);
                    } else {
                        enzyme.getConcentration()
                                .decConcentration(Concentration.MIN_CONC);
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#removeMolecule(mok.model.IMolecule)
     */
    @Override
    public ICompartment removeMolecule(final IMolecule m) {
        if (this.molecules.contains(m)) {
            // Can't use streams due to removal
            for (final IMolecule molecule : this.molecules) {
                if (molecule.equals(m)) {
                    final IConcentration c = molecule.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.molecules.remove(molecule);
                    } else {
                        molecule.getConcentration()
                                .decConcentration(Concentration.MIN_CONC);
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#removeSeed(mok.model.ISeed)
     */
    @Override
    public ICompartment removeSeed(final ISeed s) {
        if (this.seeds.contains(s)) {
            // Can't use streams due to removal
            for (final ISeed seed : this.seeds) {
                if (seed.equals(s)) {
                    final IConcentration c = seed.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.seeds.remove(seed);
                    } else {
                        seed.getConcentration()
                                .decConcentration(Concentration.MIN_CONC);
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ICompartment#removeTrace(mok.model.ITrace)
     */
    @Override
    public ICompartment removeTrace(final ITrace t) {
        if (this.traces.contains(t)) {
            // Can't use streams due to removal
            for (final ITrace trace : this.traces) {
                if (trace.equals(t)) {
                    final IConcentration c = trace.getConcentration();
                    if (c.toLong() <= Concentration.MIN_CONC) {
                        this.traces.remove(trace);
                    } else {
                        trace.getConcentration()
                                .decConcentration(Concentration.MIN_CONC);
                    }
                }
            }
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        boolean result = true;
        if (obj instanceof ICompartment) {
            final ICompartment c = (ICompartment) obj;
            if ((c.getAtoms().size() == this.atoms.size())
                    && (c.getEnzymes().size() == this.enzymes.size())
                    && (c.getMolecules().size() == this.molecules.size())
                    && (c.getSeeds().size() == this.seeds.size())
                    && (c.getTraces().size() == this.traces.size())) {
                result = !this.atoms.parallelStream()
                        .anyMatch(atom -> !c.getAtoms().contains(atom));
                if (result) {
                    result = !this.enzymes.parallelStream().anyMatch(
                            enzyme -> !c.getEnzymes().contains(enzyme));
                }
                if (result) {
                    result = !this.molecules.parallelStream().anyMatch(
                            molecule -> !c.getMolecules().contains(molecule));
                }
                if (result) {
                    result = !this.seeds.parallelStream()
                            .anyMatch(seed -> !c.getSeeds().contains(seed));
                }
                if (result) {
                    result = !this.traces.parallelStream()
                            .anyMatch(trace -> !c.getTraces().contains(trace));
                }
            } else {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.atoms.hashCode();
        res = (31 * res) + this.enzymes.hashCode();
        res = (31 * res) + this.molecules.hashCode();
        res = (31 * res) + this.seeds.hashCode();
        res = (31 * res) + this.traces.hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuffer buff = new StringBuffer();
        buff.append("compartment: [ atoms { ");
        for (final IAtom a : this.atoms) {
            buff.append(a).append(" | ");
        }
        buff.delete(buff.length() - 2, buff.length()).append(" }, enzymes { ");
        for (final IEnzyme e : this.enzymes) {
            buff.append(e).append(" | ");
        }
        buff.delete(buff.length() - 2, buff.length())
                .append(" }, molecules { ");
        for (final IMolecule m : this.molecules) {
            buff.append(m).append(" | ");
        }
        buff.delete(buff.length() - 2, buff.length()).append(" }, seeds { ");
        for (final ISeed s : this.seeds) {
            buff.append(s).append(" | ");
        }
        buff.delete(buff.length() - 2, buff.length()).append(" }, traces { ");
        for (final ITrace t : this.traces) {
            buff.append(t).append(" | ");
        }
        buff.delete(buff.length() - 2, buff.length()).append(" } ]");
        return buff.toString();
    }
}
