/**
 * Created by Stefano Mariani on 19/giu/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.io.Serializable;
import java.net.URI;
import java.util.Locale;

/**
 * <p>
 * Implementation class for {@link ISource}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Source implements ISource, Serializable {

    private static final long serialVersionUID = 1L;
    private final String description;
    private final URI uri;

    /**
     * Minimal constructor, builds {@code this} given the {@link URI} of the
     * source of information represented.
     *
     * @param u
     *            the URI of the source of information {@code this} represents.
     */
    public Source(final URI u) {
        if (u == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.uri = u;
        this.description = "";
    }

    /**
     * Complete constructor, builds {@code this} with also a description of the
     * represented source of information.
     *
     * @param u
     *            the {@link URI} of the source of information {@code this}
     *            represents.
     * @param d
     *            the description of {@code this} in {@link String} format.
     */
    public Source(final URI u, final String d) {
        if ((u == null) || (d == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.uri = u;
        this.description = d;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ISource) {
            final ISource o = (ISource) obj;
            if (o.getURI().equals(this.uri)
                    && o.getDescription().equalsIgnoreCase(this.description)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ISource#getDescription()
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.ISource#getURI()
     */
    @Override
    public URI getURI() {
        return this.uri;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.uri.hashCode();
        final String copy = new String(this.description);
        res = (31 * res) + copy.toLowerCase(Locale.ROOT).hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuffer rep = new StringBuffer();
        rep.append(String.format("source: %s", this.uri));
        if (!this.description.isEmpty()) {
            rep.append(String.format(", %s", this.description));
        }
        return rep.toString();
    }
}
