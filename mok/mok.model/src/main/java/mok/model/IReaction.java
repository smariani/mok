/**
 * Created by Stefano Mariani on 25/giu/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.Set;

/**
 * <p>
 * Interface for MoK Reactions.
 * </p>
 * <p>
 * Any MoK reaction transforms a multiset of {@link IReactantTemplate} s into a
 * multiset of {@link IProductTemplate}s at a certain {@link IRate}.
 * </p>
 *
 * @see AbstractReaction
 * @see IAggregation
 * @see IDecay
 * @see IDiffusion
 * @see IReinforcement
 * @see IInjection
 * @see IDeposit
 * @see IPerturbation
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IReaction {

    /**
     * Adds a {@link IProductTemplate} to {@code this}.
     *
     * @param productTemplate
     *            the product template to add.
     * @return {@code this}.
     */
    IReaction addProductTemplate(IProductTemplate productTemplate);

    /**
     * Adds a {@link IReactantTemplate} to {@code this}.
     *
     * @param reactantTemplate
     *            the reactant template to add.
     * @return {@code this}.
     */
    IReaction addReactantTemplate(IReactantTemplate reactantTemplate);

    /**
     * Decrements the {@link IRate} of {@code this}.
     *
     * @param decrement
     *            the rate representing the decrement.
     * @return {@code this}.
     */
    IReaction decRate(IRate decrement);

    /**
     * Gets the set of {@link IProductTemplate}s produced by {@code this}.
     *
     * @return the set of product templates produced by {@code this}.
     */
    Set<IProductTemplate> getProductTemplates();

    /**
     * Gets the {@link IRate} of execution of {@code this}.
     *
     * @return the rate of execution of {@code this}.
     */
    IRate getRate();

    /**
     * Gets the set of {@link IReactantTemplate}s produced by {@code this}.
     *
     * @return the set of reactant templates consumed by {@code this}.
     */
    Set<IReactantTemplate> getReactantTemplates();

    /**
     * Increments the {@link IRate} of {@code this}.
     *
     * @param increment
     *            the rate representing the increment.
     * @return {@code this}.
     */
    IReaction incRate(IRate increment);

    /**
     * Removes a {@link IProductTemplate} from {@code this}.
     *
     * @param productTemplate
     *            the product template to remove.
     * @return {@code this}.
     */
    IReaction removeProductTemplate(IProductTemplate productTemplate);

    /**
     * Removes a {@link IReactantTemplate} from {@code this}.
     *
     * @param reactantTemplate
     *            the reactant template to remove.
     * @return {@code this}.
     */
    IReaction removeReactantTemplate(IReactantTemplate reactantTemplate);

    /**
     * Executes {@code this}. TODO Explain better: IReaction vs IActualReaction?
     *
     * @return whether {@code this} execution succeeded or not (TODO Explain
     *         better success/failure semantics).
     */
    boolean execute();

}
