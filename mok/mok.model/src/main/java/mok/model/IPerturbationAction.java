/**
 * Created by Stefano Mariani on 20/set/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface for MoK Perturbation Actions, that is, those perturbations brought
 * by {@link ITrace}s to {@link IReactant}s through {@link IDeposit} reaction
 * and due to {@link IEnzyme}s release within {@link ICompartment}s.
 * </p>
 *
 * @see AbstractPerturbationAction
 * @see TODO Actual perturbation actions
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IPerturbationAction {

    /**
     * Applies {@code this} to the {@link IReactantTemplate}.
     *
     * @return whether {@code this} was successfully applied.
     */
    boolean apply();

    /**
     * Gets the description of {@code this}.
     *
     * @return the description of {@code this}.
     */
    String getDescription();

    /**
     * Gets the {@link IReactantTemplate} {@code this} applies to.
     *
     * @return the reactant template {@code this} applies to.
     */
    IReactantTemplate getReactantTemplate();
}
