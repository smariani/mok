/**
 * Created by Stefano Mariani on 06/giu/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.io.Serializable;

/*
 * TODO Consider thread-safety
 */
/**
 * <p>
 * Implementation class for {@link IConcentration}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Concentration
        implements IConcentration, Comparable<IConcentration>, Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Minimum concentration allowed.
     * </p>
     */
    public static final long MIN_CONC = 1;
    private long multiplicity;

    /**
     * Empty constructor, builds {@code this} with minimum admissible value.
     *
     * @see Concentration#MIN_CONC
     */
    public Concentration() {
        this.multiplicity = Concentration.MIN_CONC;
    }

    /**
     * <p>
     * PROTOTYPE IMPLEMENTATION DETAIL DISCLAIMER.
     * </p>
     * <p>
     * Complete constructor, builds {@code this} with the given initial value.
     * </p>
     *
     * @param init
     *            the initial {@code this} value.
     */
    public Concentration(final long init) {
        this.multiplicity = init;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final IConcentration c) {
        if (c == null) {
            throw new NullPointerException();
        }
        if (this.multiplicity < c.toLong()) {
            return -1;
        } else if (this.multiplicity > c.toLong()) {
            return 1;
        } else {
            return 0;
        }
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IConcentration#decConcentration(long)
     */
    @Override
    public IConcentration decConcentration(final long l) {
        if (l > this.multiplicity) {
            this.multiplicity = 0;
        } else {
            this.multiplicity -= l;
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IConcentration) {
            final IConcentration c = (IConcentration) obj;
            if (c.toLong() == this.multiplicity) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        final int c = (int) (this.multiplicity ^ (this.multiplicity >>> 32));
        res = (31 * res) + c;
        return res;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IConcentration#incConcentration(long)
     */
    @Override
    public IConcentration incConcentration(final long l) {
        this.multiplicity += l;
        return this;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IConcentration#toDouble()
     */
    @Override
    public long toLong() {
        return this.multiplicity;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.valueOf(this.multiplicity);
    }
}
