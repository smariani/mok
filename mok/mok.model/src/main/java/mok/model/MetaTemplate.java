/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.regex.Pattern;

/**
 * <p>
 * Implementation class for {@link IMetaTemplate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class MetaTemplate implements IMetaTemplate {

    private final Pattern metaTemplate;

    /**
     * Empty constructor, builds an empty template for meta-information.
     */
    public MetaTemplate() {
        this.metaTemplate = Pattern.compile("");
    }

    /**
     * Complete constructor, builds {@code this} with the given meta-information
     * template as a {@link java.util.regex.Pattern}.
     *
     * @param m
     *            the pattern representation of {@code this}.
     */
    public MetaTemplate(final Pattern m) {
        if (m == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.metaTemplate = m;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#checkMatching(mok.model.IMetaTemplate)
     */
    @Override
    public IMatchingDegree checkMatching(final IMetaTemplate wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#doMatching(mok.model.IMetaTemplate)
     */
    @Override
    public IMatch doMatching(final IMetaTemplate wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IMetaTemplate#getMetaTemplate()
     */
    @Override
    public Pattern getMetaTemplate() {
        return this.metaTemplate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IMetaTemplate) {
            final IMetaTemplate mt = (IMetaTemplate) obj;
            if (mt.getMetaTemplate().pattern()
                    .equals(this.metaTemplate.pattern())) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.metaTemplate.pattern().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.metaTemplate.pattern();
    }

}
