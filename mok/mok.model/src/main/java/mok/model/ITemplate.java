/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 * @param <T>
 *            the MoK datatype (could be {@link IContentTemplate},
 *            {@link IMetaTemplate}, {@link IPerturbationActionTemplate}),
 *            {@link ISourceTemplate}, {@link IURITemplate}.
 *
 */
public interface ITemplate<T> {

    /**
     * Checks whether the given MoK datatype complies to this template. TODO
     * Explain better how matching is done.
     *
     * @param t
     *            the MoK datatype to check.
     * @return the {@link IMatchingDegree} measuring similarity between this
     *         template and the given MoK datatype.
     */
    IMatchingDegree checkMatching(T t);

    /**
     * Performs matching between this template and a given MoK datatype.
     *
     * @param t
     *            the MoK datatype to match.
     * @return the {@link IMatch} object storing matching results. TODO Explain
     *         better what are the effect of matching.
     */
    IMatch doMatching(T t);

}
