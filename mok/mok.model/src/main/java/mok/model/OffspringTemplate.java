/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Implementation class for {@link IOffspringTemplate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class OffspringTemplate implements IOffspringTemplate {

    private final IAtomTemplate atomTemplate;
    private final IConcentration concentration;

    /**
     * Only constructor, builds {@code this} with the {@link IAtomTemplate} to
     * consider and with given minimal required {@link IConcentration}.
     *
     * @param at
     *            the atom template {@code this} considers.
     * @param c
     *            the minimal required concentration of {@code this}.
     */
    public OffspringTemplate(final IAtomTemplate at, final IConcentration c) {
        if ((at == null) || (c == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.atomTemplate = at;
        this.concentration = c;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#checkMatching(java.lang.Object)
     */
    @Override
    public IMatchingDegree checkMatching(final IOffspringTemplate t) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#doMatching(java.lang.Object)
     */
    @Override
    public IMatch doMatching(final IOffspringTemplate t) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IOffspringTemplate#getAtomTemplate()
     */
    @Override
    public IAtomTemplate getAtomTemplate() {
        return this.atomTemplate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IOffspringTemplate) {
            final IOffspringTemplate o = (IOffspringTemplate) obj;
            if (o.getAtomTemplate().equals(this.atomTemplate)) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.atomTemplate.hashCode();
        return res;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("%s * atom template: [ content: %s, meta: %s ]",
                this.concentration, this.atomTemplate.getContentTemplate(),
                this.atomTemplate.getMetaTemplate());
    }

}
