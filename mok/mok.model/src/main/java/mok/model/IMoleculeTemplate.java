/**
 * Created by Stefano Mariani on 26/giu/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.Set;

/**
 * <p>
 * Interface for {@link IMolecule} templates.
 * </p>
 *
 * @see IReactantTemplate
 * @see IProductTemplate
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IMoleculeTemplate extends IReactantTemplate, IProductTemplate {

    /**
     * Adds a {@link IAtomTemplate} to {@code this}.
     *
     * @param atomTemplate
     *            the atom template to add to {@code this}.
     * @return {@code this}.
     */
    IMoleculeTemplate addAtomTemplate(IAtomTemplate atomTemplate);

    /**
     * Adds a set of {@link IAtomTemplate}s to {@code this}.
     *
     * @param atomTemplates
     *            the set of atom templates to add to {@code this}.
     * @return {@code this}.
     */
    IMoleculeTemplate addAtomTemplates(Set<IAtomTemplate> atomTemplates);

    /**
     * Gets the set of {@link IAtomTemplate}s from {@code this}.
     *
     * @return the set of atom templates {@code this} includes.
     */
    Set<IAtomTemplate> getAtomTemplates();

    /**
     * Removes a {@link IAtomTemplate} from {@code this}.
     *
     * @param atomTemplate
     *            the atom template to remove.
     * @return {@code this}.
     */
    IMoleculeTemplate removeAtomTemplate(IAtomTemplate atomTemplate);

}
