/**
 * Created by Stefano Mariani on 28/ott/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.Locale;

/*
 * TODO Implement custom serialized form, implement clone() or factory constructor
 */
/**
 * <p>
 * Base class for {@link IPerturbationAction}s.
 * </p>
 *
 * @see TODO Actual perturbation actions
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public abstract class AbstractPerturbationAction
        implements IPerturbationAction {

    private final String description;
    private final IReactantTemplate reactantTemplate;

    /**
     * Only constructor, builds {@code this} with the given
     * {@link IReactantTemplate} and description.
     *
     * @param rt
     *            the reactant template {@code this} applies to.
     * @param d
     *            the description of {@code this}.
     */
    public AbstractPerturbationAction(final IReactantTemplate rt,
            final String d) {
        if ((rt == null) || (d == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.reactantTemplate = rt;
        this.description = d;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IPerturbation#apply()
     */
    @Override
    public abstract boolean apply();

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IPerturbationAction) {
            final IPerturbationAction pa = (IPerturbationAction) obj;
            if (pa.getReactantTemplate().equals(this.reactantTemplate)
                    && pa.getDescription().equalsIgnoreCase(this.description)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IPerturbation#getDescription()
     */
    @Override
    public final String getDescription() {
        return this.description;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IPerturbation#getReactant()
     */
    @Override
    public final IReactantTemplate getReactantTemplate() {
        return this.reactantTemplate;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public final int hashCode() {
        int res = 17;
        res = (31 * res) + this.reactantTemplate.hashCode();
        final String copy = new String(this.description);
        res = (31 * res) + copy.toLowerCase(Locale.ROOT).hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public final String toString() {
        return String.format("perturbation: %s -> ( reactant: %s )",
                this.description, this.reactantTemplate);
    }

}
