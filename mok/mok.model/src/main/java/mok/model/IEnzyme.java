/**
 * Created by Stefano Mariani on 20/set/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface for MoK Enzymes.
 * </p>
 * <p>
 * The reification of MoK users epistemic actions, enzymes have a
 * {@link ESpecies} and refer to a {@link IReactantTemplate} , have a
 * {@link IConcentration} and a "strength", also represented by a concentration
 * value.
 * </p>
 * <p>
 * The species represents the nature of the enzyme and determines the
 * {@link ITrace} the enzyme eventually deposits in the compartment.
 * </p>
 * <p>
 * The strength represents the reinforcement the enzyme brings to the reactant.
 * </p>
 *
 * @see IReactant
 * @see IProduct
 * @see IReinforcement
 * @see IDeposit
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IEnzyme extends IReactant, IProduct {

    /**
     * Decreases the strength of {@code this}' reinforcement.
     *
     * @param decrement
     *            the strength to increase {@code this}' reinforcement by.
     * @return {@code this}.
     */
    IEnzyme decStrength(IConcentration decrement);

    /**
     * Gets the {@link IReactantTemplate} {@code this} refers to.
     *
     * @return the reactant template {@code this} refers to.
     */
    IReactantTemplate getReactantTemplate();

    /**
     * Gets the {@link ESpecies} of {@code this}.
     *
     * @return the species of {@code this}.
     */
    ESpecies getSpecies();

    /**
     * Gets the strength of the reinforcement brought by {@code this}.
     *
     * @return the strength of the reinforcement.
     */
    IConcentration getStrength();

    /**
     * Increases the strength of the reinforcement of {@code this}'
     * reinforcement.
     *
     * @param increment
     *            the strength to decrease the reinforcement by.
     * @return {@code this}.
     */
    IEnzyme incStrength(IConcentration increment);

}
