/**
 * Created by Stefano Mariani on 27/mag/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Interface for MoK Atoms.
 * </p>
 * <p>
 * The atomic unit of information in MoK, composed by a {@link IContent} and
 * some {@link IMeta} information meant to ease semantic interpretation.
 * </p>
 * <p>
 * MoK atoms have a {@link IConcentration}, that is, their relative amount
 * w.r.t. other atoms in the same compartment, which affects the extent to which
 * they participate in MoK {@link IReaction}.
 * </p>
 * <p>
 * MoK atoms also originate from a {@link ISeed}, which represents a source of
 * information within MoK.
 * </p>
 *
 * @see IReactant
 * @see IProduct
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IAtom extends IReactant, IProduct {

    /**
     * Gets the {@link IContent} of {@code this}.
     *
     * @return the content of {@code this}.
     */
    IContent getContent();

    /**
     * Gets the {@link IMeta} of {@code this}.
     *
     * @return the meta information of {@code this}.
     */
    IMeta getMeta();

    /**
     * Gets the {@link ISeed} of {@code this}.
     *
     * @return the seed of {@code this}.
     */
    ISeed getSeed();

}
