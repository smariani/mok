/**
 * Created by Stefano Mariani on 18/giu/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.Set;

/**
 * <p>
 * Interface for MoK Compartments
 * </p>
 * <p>
 * The computational and topological abstraction in MoK, responsible for storing
 * {@link IAtom}s, {@link IEnzyme}s, {@link IMolecule}s, {@link ISeed}s, and
 * {@link ITrace}s, and for executing {@link IReaction}s.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface ICompartment {

    /**
     * Adds an {@link IAtom} to {@code this}.
     *
     * @param atom
     *            the atom to add.
     * @return {@code this}.
     */
    ICompartment addAtom(IAtom atom);

    /**
     * Adds an {@link IEnzyme} to {@code this}.
     *
     * @param enzyme
     *            the enzyme to add.
     * @return {@code this}.
     */
    ICompartment addEnzyme(IEnzyme enzyme);

    /**
     * Adds a {@link IMolecule} to {@code this}.
     *
     * @param molecule
     *            the molecule to add.
     * @return {@code this}.
     */
    ICompartment addMolecule(IMolecule molecule);

    /**
     * Adds a {@link ISeed} to {@code this}.
     *
     * @param seed
     *            the seed to add.
     * @return {@code this}.
     */
    ICompartment addSeed(ISeed seed);

    /**
     * Adds a {@link ITrace} to {@code this}.
     *
     * @param trace
     *            the trace to add.
     * @return {@code this}.
     */
    ICompartment addTrace(ITrace trace);

    /**
     * Gets the set of {@link IAtom}s from {@code this}.
     *
     * @return the set of atoms.
     */
    Set<IAtom> getAtoms();

    /**
     * Gets the set of {@link IEnzyme}s from {@code this}.
     *
     * @return the set of enzymes.
     */
    Set<IEnzyme> getEnzymes();

    /**
     * Gets the set of {@link IMolecule}s from {@code this}.
     *
     * @return the set of molecules.
     */
    Set<IMolecule> getMolecules();

    /**
     * Gets the set of {@link ISeed}s from {@code this}.
     *
     * @return the set of seeds.
     */
    Set<ISeed> getSeeds();

    /**
     * Gets the set of {@link ITrace}s from {@code this}.
     *
     * @return the set of traces.
     */
    Set<ITrace> getTraces();

    /**
     * Removes a {@link IAtom} from {@code this}.
     *
     * @param atom
     *            the atom to remove.
     * @return {@code this}.
     */
    ICompartment removeAtom(IAtom atom);

    /**
     * Removes a {@link IEnzyme} from {@code this}.
     *
     * @param enzyme
     *            the enzyme to remove.
     * @return {@code this}.
     */
    ICompartment removeEnzyme(IEnzyme enzyme);

    /**
     * Removes a {@link IMolecule} from {@code this}.
     *
     * @param molecule
     *            the molecule to remove.
     * @return {@code this}.
     */
    ICompartment removeMolecule(IMolecule molecule);

    /**
     * Removes a {@link ISeed} from {@code this}.
     *
     * @param seed
     *            the seed to remove.
     * @return {@code this}.
     */
    ICompartment removeSeed(ISeed seed);

    /**
     * Removes a {@link ITrace} from {@code this}.
     *
     * @param trace
     *            the trace to remove.
     * @return {@code this}.
     */
    ICompartment removeTrace(ITrace trace);

}
