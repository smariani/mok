/*
 * Copyright 1999-2019 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Implementation class for {@link IEnzymeTemplate}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 *
 */
public final class EnzymeTemplate implements IEnzymeTemplate {

    private final IConcentration concentration;
    private final IReactantTemplate reactantTemplate;
    private final ESpecies species;

    /**
     * Minimal constructor, builds {@code this} with minimal, minimal required
     * {@link IConcentration}.
     *
     * @param rt
     *            the {@link IReactantTemplate} {@code this} refers to.
     * @param s
     *            the {@link ESpecies} {@code this} belongs to.
     */
    public EnzymeTemplate(final IReactantTemplate rt, final ESpecies s) {
        if ((rt == null) || (s == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.reactantTemplate = rt;
        this.species = s;
        this.concentration = new Concentration();
    }

    /**
     * Complete constructor, builds {@code this} with given minimal required
     * {@link IConcentration}.
     *
     * @param rt
     *            the {@link IReactantTemplate} {@code this} refers to.
     * @param sp
     *            the {@link ESpecies} {@code this} belongs to.
     * @param c
     *            the minimal required {@link IConcentration} of {@code this}.
     */
    public EnzymeTemplate(final IReactantTemplate rt, final ESpecies sp,
            final IConcentration c) {
        if ((rt == null) || (sp == null) || (c == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.reactantTemplate = rt;
        this.species = sp;
        this.concentration = c;
    }

    /* (non-Javadoc)
     * @see mok.model.IReactantTemplate#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IReactantTemplate decConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#checkMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatchingDegree checkMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.ITemplate#doMatching(mok.model.WithConcentration)
     */
    @Override
    public IMatch doMatching(final WithConcentration wc) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see mok.model.IProductTemplate#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IProductTemplate incConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to method!");
        }
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /* (non-Javadoc)
     * @see mok.model.IEnzymeTemplate#getReactantTemplate()
     */
    @Override
    public IReactantTemplate getReactantTemplate() {
        return this.reactantTemplate;
    }

    /* (non-Javadoc)
     * @see mok.model.IEnzymeTemplate#getSpecies()
     */
    @Override
    public ESpecies getSpecies() {
        return this.species;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IEnzymeTemplate) {
            final IEnzymeTemplate et = (IEnzymeTemplate) obj;
            if (et.getReactantTemplate().equals(this.reactantTemplate)
                    && et.getSpecies().equals(this.species)) {
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.reactantTemplate.hashCode();
        res = (31 * res) + this.species.hashCode();
        return res;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format(
                "enzyme template: [ reactant: %s, species: %s ](%s)",
                this.reactantTemplate, this.species, this.concentration);
    }

}
