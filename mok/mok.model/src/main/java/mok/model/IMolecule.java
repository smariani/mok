/**
 * Created by Stefano Mariani on 20/set/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

import java.util.Set;

/**
 * <p>
 * Interface for MoK Molecules.
 * </p>
 * <p>
 * The composite unit of information in MoK, binds together
 * "semantically related" {@link IAtom}s.
 * </p>
 * <p>
 * MoK molecules have a {@link IConcentration}, that is, their relative amount
 * w.r.t. other molecules in the same compartment, which affects the extent to
 * which they participate in MoK {@link IReaction}.
 * </p>
 *
 * @see IReactant
 * @see IProduct
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public interface IMolecule extends IReactant, IProduct {

    /**
     * Adds a {@link IAtom} to {@code this}.
     *
     * @param atom
     *            the atom to add.
     * @return {@code this}.
     */
    IMolecule addAtom(IAtom atom);

    /**
     * Adds a set of {@link IAtom}s to {@code this}.
     *
     * @param atoms
     *            the set of atoms to add.
     * @return {@code this}.
     */
    IMolecule addAtoms(Set<IAtom> atoms);

    /**
     * Gets the set of {@link IAtom}s {@code this} binds together.
     *
     * @return the set of atoms {@code this} binds together.
     */
    Set<IAtom> getAtoms();

    /**
     * Removes the given {@link IAtom} from {@code this}.
     *
     * @param atom
     *            the atom to remove.
     * @return {@code this}.
     */
    IMolecule removeAtom(IAtom atom);
}
