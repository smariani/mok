/**
 * Created by Stefano Mariani on 19/giu/2014 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package mok.model;

/**
 * <p>
 * Implementation class for {@link IOffspring}.
 * </p>
 *
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public final class Offspring implements IOffspring {

    private final IAtom atom;
    private final IConcentration concentration;

    /**
     * Only constructor, builds {@code this} with the {@link IAtom} to generate
     * and its initial {@link IConcentration}.
     *
     * @param a
     *            the atom {@code this} eventually generates.
     * @param c
     *            the initial concetration of the generated atom.
     */
    public Offspring(final IAtom a, final IConcentration c) {
        if ((a == null) || (c == null)) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.atom = a;
        this.concentration = c;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IReactant#decConcentration(mok.model.IConcentration)
     */
    @Override
    public IOffspring decConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.concentration.decConcentration(c.toLong());
        return this;
    }

    /*
    * (non-Javadoc)
    * @see java.lang.Object#equals(java.lang.Object)
    */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IOffspring) {
            final IOffspring o = (IOffspring) obj;
            if (o.getAtom().equals(this.atom)) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IOffspring#getAtom()
     */
    @Override
    public IAtom getAtom() {
        return this.atom;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.WithConcentration#getConcentration()
     */
    @Override
    public IConcentration getConcentration() {
        return this.concentration;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int res = 17;
        res = (31 * res) + this.atom.hashCode();
        return res;
    }

    /*
     * (non-Javadoc)
     * @see mok.model.IProduct#incConcentration(mok.model.IConcentration)
     */
    @Override
    public IOffspring incConcentration(final IConcentration c) {
        if (c == null) {
            throw new IllegalArgumentException(
                    "Passed <null> argument to constructor!");
        }
        this.concentration.incConcentration(c.toLong());
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("%s * atom: [ content: %s, meta: %s ]",
                this.concentration, this.atom.getContent(),
                this.atom.getMeta());
    }
}
