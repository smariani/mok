/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of MoK <http://mok.apice.unibo.it>. MoK is free software: you can
 * redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. MoK is distributed in
 * the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details. You should have
 * received a copy of the GNU Lesser General Public License along with MoK. If
 * not, see <https://www.gnu.org/licenses/lgpl.html>.
 */
/**
 * Crated by s.mariani@unibo.it on 15/mag/2014
 */
package mok.control;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author s.mariani@unibo.it
 * 
 */
public final class PlaceHolder implements IPlaceHolder {
    /*
     * (non-Javadoc)
     * @see mok.control.IPlaceHolder#foo()
     */
    @Override
    public void foo() {
        this.log(this.toString());
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("I'm a %s", this.getClass().getSimpleName());
    }

    /**
     * @param msg
     *            the message to log
     */
    private void log(final String msg) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, msg);
    }
}
