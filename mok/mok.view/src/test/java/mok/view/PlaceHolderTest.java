/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of MoK <http://mok.apice.unibo.it>. MoK is free software: you can
 * redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. MoK is distributed in
 * the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details. You should have
 * received a copy of the GNU Lesser General Public License along with MoK. If
 * not, see <https://www.gnu.org/licenses/lgpl.html>.
 */
/**
 * Crated by s.mariani@unibo.it on 15/mag/2014
 */
package mok.view;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author s.mariani@unibo.it
 * 
 */
public class PlaceHolderTest {
    /**
     * Test method for {@link mok.view.PlaceHolder#foo()}.
     */
    @Test
    public final static void testFoo() {
        Assert.fail("Not yet implemented"); // TODO Placeholder
    }

    /**
     * Test method for {@link mok.view.PlaceHolder#toString()}.
     */
    @Test
    public final static void testToString() {
        Assert.assertTrue(new PlaceHolder().toString().equals(
                "I'm a PlaceHolder"));
    }
}
